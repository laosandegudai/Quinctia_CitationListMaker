﻿/*
 * ConfigHelper - 配置的读取类
 * 代码功能：读取 Quinctia 需要装载的所有配置
 * 作者名称：Tsybius2014
 * 创建时间：2015年1月17日
 * 最后修改：2015年1月17日
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Xml;

namespace Quinctia
{
    class ConfigHelper
    {
        /// <summary>
        /// 语言配置
        /// </summary>
        public class LanguageConfig
        {
            /// <summary>
            /// 刷新语言设置
            /// </summary>
            /// <param name="langpath"></param>
            public static void RefreshLanguageConfig(string langpath)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(langpath);

                XmlNode root = xmlDoc.SelectSingleNode(ConstantDict.CONSTANT_LANG_LANGUAGE);
                foreach (XmlNode xnLang in root.ChildNodes)
                {
                     //只有读取到XmlElement才进一步读取
                    if (xnLang.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeLang = (XmlElement)xnLang;

                        switch (xeLang.Name.Trim())
                        {
                            case ConstantDict.CONSTANT_LANG_FORM_MAIN: SetFormMain(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_SELECT_FROM_TABLE: SetFormSelectFromTable(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_ABOUT: SetFormAbout(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_SELECT_LANGUAGE: SetFormSelectLanguage(xeLang); break;

                            case ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH: SetFormMonograph(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION: SetFormMonographContribution(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_SERIAL: SetFormSerial(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION: SetFormSerialContribution(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT: SetFormPatentDocument(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT: SetFormElectronicDocument(xeLang); break;

                            case ConstantDict.CONSTANT_LANG_DGV_LIT_CARRIER_TYPE: SetDgvColumnCarrierType(xeLang); break;
                            case ConstantDict.CONSTANT_LANG_NODE_ERRORS: SetErrors(xeLang); break;
                            default: break;
                        }
                    }
                }
            }

            #region 主窗体
            /// <summary>
            /// 主窗体
            /// </summary>
            /// <param name="xeLang"></param>
            public static void SetFormMain(XmlElement xeLang)
            {
                langFormMain.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMI) //识别到菜单
                        {
                            foreach (XmlNode xnTsmi in xeItem.ChildNodes)
                            {
                                if (xnTsmi.GetType().ToString() == "System.Xml.XmlElement")
                                {
                                    XmlElement xeTsmi = (XmlElement)xnTsmi;

                                    if (xeTsmi.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF)
                                    {
                                        langFormMain.strToolStripMenuItemFileText = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                        langFormMain.strToolStripMenuItemFileNew = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF_NEW).Trim();
                                        langFormMain.strToolStripMenuItemFileOpen = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF_OPEN).Trim();
                                        langFormMain.strToolStripMenuItemFileSave = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF_SAVE).Trim();
                                        langFormMain.strToolStripMenuItemFileGenerate = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF_GENERATE).Trim();
                                        langFormMain.strToolStripMenuItemFileConfig = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF_CONFIG).Trim();
                                        langFormMain.strToolStripMenuItemFileExit = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIF_EXIT).Trim();
                                    }
                                    else if (xeTsmi.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIH)
                                    {
                                        langFormMain.strToolStripMenuItemHelpText = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                        langFormMain.strToolStripMenuItemHelpHelp = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIH_HELP).Trim();
                                        langFormMain.strToolStripMenuItemHelpAbout = xeTsmi.GetAttribute(ConstantDict.CONSTANT_LANG_FORM_MAIN_TSMIH_ABOUT).Trim();
                                    }
                                }
                            }
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_CD)
                        {
                            langFormMain.strGroupBoxCurrDataText = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD)
                        {
                            langFormMain.strGroupBoxAddDataText = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();

                            foreach (XmlNode xnButton in xeItem.ChildNodes)
                            {
                                if (xnButton.GetType().ToString() == "System.Xml.XmlElement")
                                {
                                    XmlElement xeButton = (XmlElement)xnButton;

                                    if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD_M)
                                    {
                                        langFormMain.strAddMonographText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD_MC)
                                    {
                                        langFormMain.strAddMonographContributionText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD_S)
                                    {
                                        langFormMain.strAddSerialText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD_SC)
                                    {
                                        langFormMain.strAddSerialContributionText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD_PD)
                                    {
                                        langFormMain.strAddPatentDocumentationText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_AD_ED)
                                    {
                                        langFormMain.strAddElectronicDocumentText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                }
                            }
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_OO)
                        {
                            langFormMain.strGroupBoxOtherOperText = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();

                            foreach (XmlNode xnButton in xeItem.ChildNodes)
                            {
                                if (xnButton.GetType().ToString() == "System.Xml.XmlElement")
                                {
                                    XmlElement xeButton = (XmlElement)xnButton;

                                    if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_OO_CLEAR)
                                    {
                                        langFormMain.strClearSelectedText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_OO_MODIFY)
                                    {
                                        langFormMain.strModifySelectedText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_OO_DELETE)
                                    {
                                        langFormMain.strDeleteSelectedText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                    else if (xeButton.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_OO_GENERATE)
                                    {
                                        langFormMain.strGenerateText = xeButton.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                                    }
                                }
                            }
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MAIN_SS)
                        {
                            langFormMain.strStatusStripText = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion

            #region 专著
            public static void SetFormMonograph(XmlElement xeLang)
            {
                langFormMonograph.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);
                
                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_TXT_INTRODUCTION)
                        {
                            langFormMonograph.strIntroduction = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_BTN_OK)
                        {
                            langFormMonograph.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_BTN_CANCEL)
                        {
                            langFormMonograph.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_MAIN_PEOPLE)
                        {
                            langFormMonograph.strMainPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_TITLE)
                        {
                            langFormMonograph.strXTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_TITLE_TIP)
                        {
                            langFormMonograph.strTitleTip = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_LIT_TYPE)
                        {
                            langFormMonograph.strLiteratureType = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_OTHER_PEOPLE)
                        {
                            langFormMonograph.strOtherPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_VERSION)
                        {
                            langFormMonograph.strVersion = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_PUB_PLACE)
                        {
                            langFormMonograph.strPubPlace = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_PUB_PEOPLE)
                        {
                            langFormMonograph.strPubPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_PUB_YEAR)
                        {
                            langFormMonograph.strPubYear = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_REF_PAGE)
                        {
                            langFormMonograph.strRefPage = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_REF_TIME)
                        {
                            langFormMonograph.strRefTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_LBL_PATH)
                        {
                            langFormMonograph.strPath = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
            #region 专著析出文献

            public static void SetFormMonographContribution(XmlElement xeLang)
            {
                langFormMonographContribution.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_TXT_INTRODUCTION)
                        {
                            langFormMonographContribution.strIntroduction = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_BTN_OK)
                        {
                            langFormMonographContribution.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_BTN_CANCEL)
                        {
                            langFormMonographContribution.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_MAIN_PEOPLE)
                        {
                            langFormMonographContribution.strMainPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_CONT_TITLE)
                        {
                            langFormMonographContribution.strContributionTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_LIT_TYPE)
                        {
                            langFormMonographContribution.strLiteratureType = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_OTHER_PEOPLE)
                        {
                            langFormMonographContribution.strOtherPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_MONO_MAIN_PEOPLE)
                        {
                            langFormMonographContribution.strMonoMainPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_MONO_TITLE)
                        {
                            langFormMonographContribution.strMonoTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_TITLE_INFO)
                        {
                            langFormMonographContribution.strTitleInfo = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_VERSION)
                        {
                            langFormMonographContribution.strVersion = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PUB_PLACE)
                        {
                            langFormMonographContribution.strPubPlace = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PUB_PEOPLE)
                        {
                            langFormMonographContribution.strPubPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PUB_YEAR)
                        {
                            langFormMonographContribution.strPubYear = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_REF_PAGE)
                        {
                            langFormMonographContribution.strRefPage = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_REF_TIME)
                        {
                            langFormMonographContribution.strRefTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PATH)
                        {
                            langFormMonographContribution.strPath = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
            #region 连续出版物

            public static void SetFormSerial(XmlElement xeLang)
            {
                langFormSerial.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_TXT_INTRODUCTION)
                        {
                            langFormSerial.strIntroduction = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_BTN_OK)
                        {
                            langFormSerial.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_BTN_CANCEL)
                        {
                            langFormSerial.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_MAIN_PEOPLE)
                        {
                            langFormSerial.strMainPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_TITLE)
                        {
                            langFormSerial.strXTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_TITLE_TIP)
                        {
                            langFormSerial.strTitleTip = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_LIT_TYPE)
                        {
                            langFormSerial.strLiteratureType = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_POSITION)
                        {
                            langFormSerial.strPosition = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_PUB_PLACE)
                        {
                            langFormSerial.strPubPlace = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_PUB_PEOPLE)
                        {
                            langFormSerial.strPubPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_PUB_YEAR)
                        {
                            langFormSerial.strPubYear = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_REF_TIME)
                        {
                            langFormSerial.strRefTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_LBL_PATH)
                        {
                            langFormSerial.strPath = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
            #region 连续出版物析出文献

            public static void SetFormSerialContribution(XmlElement xeLang)
            {
                langFormSerialContribution.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_TXT_INTRODUCTION)
                        {
                            langFormSerialContribution.strIntroduction = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_BTN_OK)
                        {
                            langFormSerialContribution.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_BTN_CANCEL)
                        {
                            langFormSerialContribution.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }

                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_MAIN_PEOPLE)
                        {
                            langFormSerialContribution.strMainPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_TITLE)
                        {
                            langFormSerialContribution.strXTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_LIT_TYPE)
                        {
                            langFormSerialContribution.strLiteratureType = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_SERIAL_NAME)
                        {
                            langFormSerialContribution.strSerialName = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_SERIAL_INFO)
                        {
                            langFormSerialContribution.strSerialInfo = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_POSITION)
                        {
                            langFormSerialContribution.strPosition = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_REF_TIME)
                        {
                            langFormSerialContribution.strRefTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_PATH)
                        {
                            langFormSerialContribution.strPath = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
            #region 专利文献

            public static void SetFormPatentDocument(XmlElement xeLang)
            {
                langFormPatentDocument.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_TXT_INTRODUCTION)
                        {
                            langFormPatentDocument.strIntroduction = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENTL_BTN_OK)
                        {
                            langFormPatentDocument.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_BTN_CANCEL)
                        {
                            langFormPatentDocument.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_OWNER)
                        {
                            langFormPatentDocument.strOwner = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_TITLE)
                        {
                            langFormPatentDocument.strXTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_COUNTRY)
                        {
                            langFormPatentDocument.strCountry = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_PN)
                        {
                            langFormPatentDocument.strPN = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_LIT_TYPE)
                        {
                            langFormPatentDocument.strLiteratureType = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_OPEN_TIME)
                        {
                            langFormPatentDocument.strOpenTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_REF_TIME)
                        {
                            langFormPatentDocument.strRefTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_PATH)
                        {
                            langFormPatentDocument.strPath = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
            #region 电子文献

            public static void SetFormElectronicDocument(XmlElement xeLang)
            {
                langFormElectronicDocument.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_TXT_INTRODUCTION)
                        {
                            langFormElectronicDocument.strIntroduction = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENTL_BTN_OK)
                        {
                            langFormElectronicDocument.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_BTN_CANCEL)
                        {
                            langFormElectronicDocument.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_MAIN_PEOPLE)
                        {
                            langFormElectronicDocument.strMainPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_TITLE)
                        {
                            langFormElectronicDocument.strXTitle = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_TITLE_TIP)
                        {
                            langFormElectronicDocument.strTitleTip = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_LIT_TYPE)
                        {
                            langFormElectronicDocument.strLiteratureType = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PUB_PLACE)
                        {
                            langFormElectronicDocument.strPubPlace = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PUB_PEOPLE)
                        {
                            langFormElectronicDocument.strPubPeople = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PUB_YEAR)
                        {
                            langFormElectronicDocument.strPubYear = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_LAST_CHANGE)
                        {
                            langFormElectronicDocument.strLastChange = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_REF_TIME)
                        {
                            langFormElectronicDocument.strRefTime = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PATH)
                        {
                            langFormElectronicDocument.strPath = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion

            #region 从表格中选去数据行窗口

            /// <summary>
            /// 从表格中选去数据行窗口
            /// </summary>
            /// <param name="xeLang"></param>
            public static void SetFormSelectFromTable(XmlElement xeLang)
            {
                langFormSelectFromTable.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT);
                
                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SELECT_FROM_TABLE_GRB_TABLE)
                        {
                            langFormSelectFromTable.strTip = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SELECT_FROM_TABLE_BTN_OK)
                        {
                            langFormSelectFromTable.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SELECT_FROM_TABLE_BTN_CANCEL)
                        {
                            langFormSelectFromTable.strCancel = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
   
            #region 关于窗口

            /// <summary>
            /// 关于窗口
            /// </summary>
            /// <param name="xeLang"></param>
            public static void SetFormAbout(XmlElement xeLang)
            {
                langFormAbout.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ABOUT_TOPIC)
                        {
                            langFormAbout.strTopic = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ABOUT_ABOUT)
                        {
                            langFormAbout.strAbout = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_ABOUT_OK)
                        {
                            langFormAbout.strOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion
            #region 选则语言窗口

            /// <summary>
            /// 
            /// </summary>
            /// <param name="xeLang"></param>
            public static void SetFormSelectLanguage(XmlElement xeLang)
            {
                langFormSelectLanguage.strTitle = xeLang.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();

                //读取程序支持的语言包
                foreach (XmlNode xnItem in xeLang.ChildNodes)
                {
                    if (xnItem.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeItem = (XmlElement)xnItem;

                        if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SELECT_LANGUAGE_TIP)
                        {
                            langFormSelectLanguage.strLabelTip = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SELECT_LANGUAGE_NO_PROMPT)
                        {
                            langFormSelectLanguage.strCheckBoxNoPrompt = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                        else if (xeItem.Name == ConstantDict.CONSTANT_LANG_FORM_SELECT_LANGUAGE_OK)
                        {
                            langFormSelectLanguage.strButtonOK = xeItem.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT).Trim();
                        }
                    }
                }
            }

            #endregion

            #region 读取文献载体代码表列名 文献载体代码表列名

            /// <summary>
            /// 读取文献载体代码表列名 文献载体代码表列名
            /// </summary>
            /// <param name="xeLang"></param>
            public static void SetDgvColumnCarrierType(XmlElement xeLang)
            {
                //读取程序支持的语言包
                foreach (XmlNode xnColumn in xeLang.ChildNodes)
                {
                    if (xnColumn.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeColumn = (XmlElement)xnColumn;

                        if (xeColumn.Name == ConstantDict.CONSTANT_LANG_COLUMN)
                        {
                            switch (xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_TEXT))
                            {
                                case ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_ID:
                                    {
                                        langDgvColumnCarrierType.strColumnNameId = xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_NAME);
                                    }
                                    break;
                                case ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_NAME:
                                    {
                                        langDgvColumnCarrierType.strColumnNameName = xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_NAME);
                                    }
                                    break;
                                case ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_SHORT_NAME:
                                    {
                                        langDgvColumnCarrierType.strColumnNameShortName = xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_NAME);
                                    }
                                    break;
                                case ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_DOUBLE_WORD:
                                    {
                                        langDgvColumnCarrierType.strColumnNameDoubleWord = xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_NAME);
                                    }
                                    break;
                                case ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_SINGLE_WORD:
                                    {
                                        langDgvColumnCarrierType.strColumnNameSingleWord = xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_NAME);
                                    }
                                    break;
                                case ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_COMMENT:
                                    {
                                        langDgvColumnCarrierType.strColumnNameComment = xeColumn.GetAttribute(ConstantDict.CONSTANT_LANG_NAME);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

            #endregion
            #region 读取报错信息

            /// <summary>
            /// 
            /// </summary>
            /// <param name="xeLang"></param>
            public static void SetErrors(XmlElement xeLang)
            {
                dtErrorData.Clear();
                dtErrorData.Columns.Clear();
                dtErrorData.Columns.Add("Code");
                dtErrorData.Columns.Add("Info");

                //读取程序支持的语言包
                foreach (XmlNode xnError in xeLang.ChildNodes)
                {
                    if (xnError.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeError = (XmlElement)xnError;

                        if (xeError.Name == ConstantDict.CONSTANT_LANG_NODE_ERROR)
                        {
                            dtErrorData.Rows.Add(
                                xeError.GetAttribute(ConstantDict.CONSTANT_LANG_ATTRIBUTE_CODE).Trim(),
                                xeError.GetAttribute(ConstantDict.CONSTANT_LANG_ATTRIBUTE_INFO).Trim());
                        }
                    }
                }
            }

            #endregion

            #region 常量表

            /// <summary>
            /// 主界面
            /// </summary>
            public struct langFormMain
            {
                public static string strTitle = "";

                public static string strToolStripMenuItemFileText = "";
                public static string strToolStripMenuItemFileNew = "";
                public static string strToolStripMenuItemFileOpen = "";
                public static string strToolStripMenuItemFileSave = "";
                public static string strToolStripMenuItemFileGenerate = "";
                public static string strToolStripMenuItemFileConfig = "";
                public static string strToolStripMenuItemFileExit = "";

                public static string strToolStripMenuItemHelpText = "";
                public static string strToolStripMenuItemHelpHelp = "";
                public static string strToolStripMenuItemHelpAbout = "";

                public static string strGroupBoxCurrDataText = "";

                public static string strGroupBoxAddDataText = "";
                public static string strAddMonographText = "";
                public static string strAddMonographContributionText = "";
                public static string strAddSerialText = "";
                public static string strAddSerialContributionText = "";
                public static string strAddPatentDocumentationText = "";
                public static string strAddElectronicDocumentText = "";

                public static string strGroupBoxOtherOperText = "";
                public static string strClearSelectedText = "";
                public static string strModifySelectedText = "";
                public static string strDeleteSelectedText = "";
                public static string strGenerateText = "";

                public static string strStatusStripText = "";
            }
            
            /// <summary>
            /// DataTable中选择DataRow界面
            /// </summary>
            public struct langFormSelectFromTable
            {
                public static string strTitle = "";
                public static string strTip = "";
                public static string strOK = "";
                public static string strCancel = "";
            }

            public struct langFormMonograph
            {
                public static string strTitle = "";
                public static string strIntroduction = "";
                public static string strOK = "";
                public static string strCancel = "";

                public static string strMainPeople = "";
                public static string strXTitle = "";
                public static string strTitleTip = "";
                public static string strLiteratureType = "";
                public static string strOtherPeople = "";
                public static string strVersion = "";
                public static string strPubPlace = "";
                public static string strPubPeople = "";
                public static string strPubYear = "";
                public static string strRefPage = "";
                public static string strRefTime = "";
                public static string strPath = "";
            }

            public struct langFormMonographContribution
            {
                public static string strTitle = "";
                public static string strIntroduction = "";
                public static string strOK = "";
                public static string strCancel = "";

                public static string strMainPeople = "";
                public static string strContributionTitle = "";
                public static string strLiteratureType = "";
                public static string strOtherPeople = "";
                public static string strMonoMainPeople = "";
                public static string strMonoTitle = "";
                public static string strTitleInfo = "";
                public static string strVersion = "";
                public static string strPubPlace = "";
                public static string strPubPeople = "";
                public static string strPubYear = "";
                public static string strRefPage = "";
                public static string strRefTime = "";
                public static string strPath = "";
            }

            public struct langFormSerial
            {
                public static string strTitle = "";
                public static string strIntroduction = "";
                public static string strOK = "";
                public static string strCancel = "";

                public static string strMainPeople = "";
                public static string strXTitle = "";
                public static string strTitleTip = "";
                public static string strLiteratureType = "";
                public static string strPosition = "";
                public static string strPubPlace = "";
                public static string strPubPeople = "";
                public static string strPubYear = "";
                public static string strRefTime = "";
                public static string strPath = "";
            }

            public struct langFormSerialContribution
            {
                public static string strTitle = "";
                public static string strIntroduction = "";
                public static string strOK = "";
                public static string strCancel = "";

                public static string strMainPeople = "";
                public static string strXTitle = "";
                public static string strLiteratureType = "";
                public static string strSerialName = "";
                public static string strSerialInfo = "";
                public static string strPosition = "";
                public static string strRefTime = "";
                public static string strPath = "";
            }

            public struct langFormPatentDocument
            {
                public static string strTitle = "";
                public static string strIntroduction = "";
                public static string strOK = "";
                public static string strCancel = "";

                public static string strOwner = "";
                public static string strXTitle = "";
                public static string strCountry = "";
                public static string strPN = "";
                public static string strLiteratureType = "";
                public static string strOpenTime = "";
                public static string strRefTime = "";
                public static string strPath = "";
            }

            public struct langFormElectronicDocument
            {
                public static string strTitle = "";
                public static string strIntroduction = "";
                public static string strOK = "";
                public static string strCancel = "";

                public static string strMainPeople = "";
                public static string strXTitle = "";
                public static string strTitleTip = "";
                public static string strLiteratureType = "";
                public static string strPubPlace = "";
                public static string strPubPeople = "";
                public static string strPubYear = "";
                public static string strLastChange = "";
                public static string strRefTime = "";
                public static string strPath = "";
            }

            /// <summary>
            /// 关于界面
            /// </summary>
            public struct langFormAbout
            {
                public static string strTitle = "";
                public static string strTopic = "";
                public static string strAbout = "";
                public static string strOK = "";
            }

            /// <summary>
            /// 选择语言界面
            /// </summary>
            public struct langFormSelectLanguage
            {
                public static string strTitle = "";
                public static string strLabelTip = "";
                public static string strCheckBoxNoPrompt = "";
                public static string strButtonOK = "";
            }

            /// <summary>
            /// 文献载体代码表列名 文献载体代码表列名
            /// </summary>
            public struct langDgvColumnCarrierType
            {
                public static string strColumnNameId = "";
                public static string strColumnNameName = "";
                public static string strColumnNameShortName = "";
                public static string strColumnNameDoubleWord = "";
                public static string strColumnNameSingleWord = "";
                public static string strColumnNameComment = "";
            }

            #endregion

            #region 错误信息处理

            /// <summary>
            /// 错误信息表
            /// </summary>
            private static DataTable dtErrorData = new DataTable();
            /// <summary>
            /// 错误信息表
            /// </summary>
            /// <returns></returns>
            public static DataTable GetErrorData()
            {
                return dtErrorData.Copy();
            }

            /// <summary>
            /// 从配置文件中找出错误号对应的错误信息，没有则返回空
            /// </summary>
            /// <param name="exceptioncode">错误号</param>
            /// <returns></returns>
            public static string GetErrorInfo(string exceptioncode)
            {
                if (dtErrorData.Columns.Contains(ConstantDict.CONSTANT_LANG_ATTRIBUTE_CODE) &&
                    dtErrorData.Columns.Contains(ConstantDict.CONSTANT_LANG_ATTRIBUTE_INFO))
                {
                    foreach (DataRow drException in dtErrorData.Rows)
                    {
                        if (drException[ConstantDict.CONSTANT_LANG_ATTRIBUTE_CODE].ToString() == exceptioncode)
                        {
                            return drException[ConstantDict.CONSTANT_LANG_ATTRIBUTE_INFO].ToString();
                        }
                    }
                }

                //未找到匹配错误号则返回空
                return "";
            }

            #endregion
        }

        /// <summary>
        /// 程序总配置
        /// </summary>
        public class QuinctiaConfig
        {
            /// <summary>
            /// 当前使用的语言
            /// </summary>
            private static string strCurrLangPath = "";
            /// <summary>
            /// 当前使用的语言
            /// </summary>
            /// <returns></returns>
            public static string GetCurrLangPath()
            {
                return strCurrLangPath;
            }

            private static string strLiteratureCarrierPath = "";
            /// <summary>
            /// 文献载体代码配置文件路径
            /// </summary>
            /// <returns></returns>
            public static string GetLiteratureCarrierPath()
            {
                return strLiteratureCarrierPath;
            }

            private static string strLiteratureTypePath = "";
            /// <summary>
            /// 文献类型代码配置文件路径
            /// </summary>
            /// <returns></returns>
            public static string GetLiteratureTypePath()
            {
                return strLiteratureTypePath;
            }

            /// <summary>
            /// 指示是否需要在程序启动前用提示框询问语言
            /// </summary>
            private static bool bNeedLanguagePrompt = true;
            /// <summary>
            /// 指示是否需要在程序启动前用提示框询问语言
            /// </summary>
            /// <returns></returns>
            public static bool GetNeedLanguagePrompt()
            {
                return bNeedLanguagePrompt;
            }

            /// <summary>
            /// 语言文件对照表
            /// </summary>
            public static DataTable dtLang = new DataTable();
            /// <summary>
            /// 语言文件对照表
            /// </summary>
            /// <returns></returns>
            public static DataTable GetLang()
            {
                return dtLang.Copy();
            }

            /// <summary>
            /// 刷新Quinctia程序设置
            /// </summary>
            public static void RefreshQuinctiaConfig()
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(ConstantDict.CONSTANT_CONFIG_FILENAME);

                XmlNode root = xmlDoc.SelectSingleNode(ConstantDict.CONSTANT_CONFIG_NODE_QUINCTIA_CONFIG);
                foreach (XmlNode xnConfig in root.ChildNodes)
                {
                    //只有读取到XmlElement才进一步读取
                    if (xnConfig.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeConfig = (XmlElement)xnConfig;

                        //读取语言设置
                        if (xeConfig.Name == ConstantDict.CONSTANT_CONFIG_NODE_LANG_CONFIG)
                        {
                            //获取当前正在使用的语言
                            strCurrLangPath = xeConfig.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_CURR_LANG);

                            //是否需要在程序刚启动时提示语言选项
                            if (xeConfig.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NEED_PROMPT).ToLower().Trim() == "yes" ||
                                xeConfig.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NEED_PROMPT).ToLower().Trim() == "y")
                            {
                                bNeedLanguagePrompt = true;
                            }
                            else
                            {
                                bNeedLanguagePrompt = false;
                            }

                            dtLang.Clear();
                            dtLang.Columns.Clear();
                            dtLang.Columns.Add(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NAME); //语言名称
                            dtLang.Columns.Add(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_FILE); //语言文件

                            //读取程序支持的语言包
                            foreach (XmlNode xnLang in xeConfig.ChildNodes)
                            {
                                if (xnLang.GetType().ToString() == "System.Xml.XmlElement")
                                {
                                    XmlElement xeLang = (XmlElement)xnLang;

                                    if (xeLang.Name == ConstantDict.CONSTANT_CONFIG_NODE_LANG)
                                    {
                                        dtLang.Rows.Add(
                                            xeLang.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NAME).Trim(), 
                                            xeLang.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_FILE).Trim());
                                    }
                                }
                            }
                        }
                        //获取文献载体代码表和文献类型代码表的配置文件地址
                        else if (xeConfig.Name == ConstantDict.CONSTANT_CONFIG_NODE_LITERATURE_CODE_CONFIG)
                        {
                            strLiteratureCarrierPath = xeConfig.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_CARRIER);
                            strLiteratureTypePath = xeConfig.GetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_TYPE);
                        }
                    }
                }
            }

            /// <summary>
            /// 设置配置中的语言信息
            /// </summary>
            /// <param name="langpath">语言包文件地址</param>
            /// <param name="needprompt">是否需要提示</param>
            public static void SetLanguage(string langpath, bool needprompt)
            {
                //更新当前的配置
                strCurrLangPath = langpath; 
                bNeedLanguagePrompt = needprompt; 

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(ConstantDict.CONSTANT_CONFIG_FILENAME);
                
                XmlNode root = xmlDoc.SelectSingleNode(ConstantDict.CONSTANT_CONFIG_NODE_QUINCTIA_CONFIG);
                foreach (XmlNode xnConfig in root.ChildNodes)
                {
                    //只有读取到XmlElement才进一步读取
                    if (xnConfig.GetType().ToString() == "System.Xml.XmlElement")
                    {
                        XmlElement xeConfig = (XmlElement)xnConfig;

                        //读取语言设置
                        if (xeConfig.Name == ConstantDict.CONSTANT_CONFIG_NODE_LANG_CONFIG)
                        {
                            xeConfig.SetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_CURR_LANG, langpath);
                            xeConfig.SetAttribute(ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NEED_PROMPT, needprompt ? "yes" : "no");
                        }
                    }
                }

                xmlDoc.Save(ConstantDict.CONSTANT_CONFIG_FILENAME);
            }
        }
    }
}
