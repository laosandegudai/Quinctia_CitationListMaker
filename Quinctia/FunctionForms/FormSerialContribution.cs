﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.FunctionForms
{
    public partial class FormSerialContribution : Form
    {
        public FormSerialContribution()
        {
            InitializeComponent();
        }

        public FormSerialContribution(RefSerialContribution refSerialContribution)
        {
            InitializeComponent();
            SetTxtControls(refSerialContribution);
        }

        private void SetTxtControls(RefSerialContribution refSerialContribution)
        {
            this.txtMainPeople.Text = refSerialContribution.strMainPeople;
            this.txtTitle.Text = refSerialContribution.strTitle;
            this.cmbLiteratureType.Text = refSerialContribution.strLiteratureType;
            this.cmbLiteratureCarrier.Text = refSerialContribution.strLiteratureCarrier;
            this.txtSerialName.Text = refSerialContribution.strSerialName;
            this.txtSerialInfo.Text = refSerialContribution.strSerialInfo;
            this.txtPosition.Text = refSerialContribution.strPosition;
            this.txtRefTime.Text = refSerialContribution.strRefTime;
            this.txtPath.Text = refSerialContribution.strPath;
        }

        /// <summary>
        /// 时钟，因为在ComboBox的SelectedIndexChanged事件中无法再次
        /// 改变ComboBox的值，因此需要通过这个时钟在10毫秒后改变
        /// </summary>
        Timer tmrRefreshComboBox = new Timer();

        /// <summary>
        /// 控件cmbLiteratureType的刷新用值
        /// </summary>
        string strLiteratureType = "[M] 专著";
        /// <summary>
        /// 控件cmbLiteratureCarrier的刷新用值
        /// </summary>
        string strLiteratureCarrier = "[  ] 不填写";

        private void FormSerialContribution_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetControlData();
            SetComboBoxTimer();
        }
        
        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strTitle;
            this.txtIntroduction.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strIntroduction.Replace("|", "\r\n"); ;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strCancel;

            this.lblMainPeople.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strMainPeople.Trim();
            this.lblTitle.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strXTitle.Trim();
            this.lblLiteratureType.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strLiteratureType.Trim();
            this.lblSerialName.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strSerialName.Trim();
            this.lblSerialInfo.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strSerialInfo.Trim();
            this.lblPosition.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strPosition.Trim();
            this.lblRefTime.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strRefTime.Trim();
            this.lblPath.Text = ConfigHelper.LanguageConfig.langFormSerialContribution.strPath.Trim();
        }

        private void SetControlData()
        {
            cmbLiteratureType.Items.Add("[M] 专著");
            cmbLiteratureType.Items.Add("[J] 期刊");
            cmbLiteratureType.Items.Add("[D] 学位论文");
            cmbLiteratureType.Items.Add("[.] 更多类型");
            cmbLiteratureType.Text = cmbLiteratureType.Items[0].ToString();

            cmbLiteratureCarrier.Items.Add("[  ] 不填写");
            cmbLiteratureCarrier.Items.Add("[CD] 光盘");
            cmbLiteratureCarrier.Items.Add("[OL] 联机网络");
            cmbLiteratureCarrier.Items.Add("[..] 更多载体");
            cmbLiteratureCarrier.Text = cmbLiteratureCarrier.Items[0].ToString();
        }

        /// <summary>
        /// 设置时钟 tmrRefreshComboBox
        /// </summary>
        private void SetComboBoxTimer()
        {
            this.tmrRefreshComboBox.Enabled = true;
            this.tmrRefreshComboBox.Interval = 10;
            this.tmrRefreshComboBox.Tick += (obj, arg) =>
            {
                this.cmbLiteratureType.Text = strLiteratureType;
                this.cmbLiteratureCarrier.Text = strLiteratureCarrier;
                this.tmrRefreshComboBox.Stop();
            };
        }

        public RefSerialContribution refSerialContribution = null;

        /// <summary>
        /// 设置生成的结果集
        /// </summary>
        private void SetRefSerialContribution()
        {
            refSerialContribution = new RefSerialContribution();
            refSerialContribution.strRefType = ConstantDict.CONSTANT_REF_TYPE_SERIAL_CONTRIBUTION;
            refSerialContribution.strId = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            refSerialContribution.strMainPeople = this.txtMainPeople.Text.Trim();
            refSerialContribution.strTitle = this.txtTitle.Text.Trim();
            refSerialContribution.strLiteratureType = this.cmbLiteratureType.Text.Trim();
            refSerialContribution.strLiteratureCarrier = this.cmbLiteratureCarrier.Text.Trim();
            refSerialContribution.strSerialName = this.txtSerialName.Text.Trim();
            refSerialContribution.strSerialInfo = this.txtSerialInfo.Text.Trim();
            refSerialContribution.strPosition = this.txtPosition.Text.Trim();
            refSerialContribution.strRefTime = this.txtRefTime.Text.Trim();
            refSerialContribution.strPath = this.txtPath.Text.Trim();
        }

        /// <summary>
        /// 输入合法性检查
        /// </summary>
        /// <param name="errorcode"></param>
        /// <returns></returns>
        private bool ValidityCheck(out string errorcode)
        {
            errorcode = "";

            if (string.IsNullOrWhiteSpace(txtMainPeople.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_MAIN_PEOPLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_TITLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtSerialName.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_SERIAL_TITLE;
                return false;
            }

            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorcode = "";
            if (ValidityCheck(out errorcode))
            {
                SetRefSerialContribution();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(new ErrorManip.ErrorData(errorcode).ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLiteratureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多类型时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureType.Text == "[.] 更多类型")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureTypePath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_TYPE]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureType = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureType = this.cmbLiteratureType.Text;
            }
        }

        private void cmbLiteratureCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多载体时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureCarrier.Text == "[..] 更多载体")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureCarrierPath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_CARRIER]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureCarrier = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            }
        }

        private void cmbLiteratureType_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }

        private void cmbLiteratureCarrier_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }
    }
}
