﻿/*
 * FormSelectFromTable - 从一个表中选取某一项
 * 代码功能：Quinctia 的选择窗口
 * 作者名称：Tsybius2014
 * 创建时间：2015年1月18日
 * 最后修改：2015年1月18日
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.FunctionForms
{
    public partial class FormSelectFromTable : Form
    {
        public FormSelectFromTable()
        {
            InitializeComponent();

            //这句话必须放在构造函数中，不能放在Load函数中
            //this.DialogResult = DialogResult.Cancel; 
        }

        public FormSelectFromTable(DataTable dtData)
        {
            InitializeComponent();
            this.dtData = dtData;

            //这句话必须放在构造函数中，不能放在Load函数中
            //this.DialogResult = DialogResult.Cancel;
        }

        private void FormSelectFromTable_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig(); //加载语言
            SetData(); //加载数据

        }

        DataTable dtData = new DataTable();

        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormSelectFromTable.strTitle;
            this.grbTable.Text = ConfigHelper.LanguageConfig.langFormSelectFromTable.strTip;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormSelectFromTable.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormSelectFromTable.strCancel;
        }

        private void SetData()
        {
            //按语言设置列头
            dtData.Columns[ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_ID].ColumnName = ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameId;
            dtData.Columns[ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_NAME].ColumnName = ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameName;
            dtData.Columns[ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_SHORT_NAME].ColumnName = ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameShortName;
            dtData.Columns[ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_DOUBLE_WORD].ColumnName = ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameDoubleWord;
            dtData.Columns[ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_SINGLE_WORD].ColumnName = ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameSingleWord;
            dtData.Columns[ConstantDict.CONSTANT_LANG_DGV_LIT_COLUMN_COMMENT].ColumnName = ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameComment;

            this.dgvData.DataSource = dtData;

            //单击单元格时选取整行
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToResizeRows = false;
            
            //设置表格列宽自适应
            for (int i = 0; i < dgvData.Columns.Count; i++)
            {
                dgvData.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

        /// <summary>
        /// 外面读取的返回值
        /// </summary>
        /// <returns></returns>
        public string GetCurrentSelection()
        {
            int intIndexSingleWord = dgvData.Columns[ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameSingleWord].Index;
            int intIndexName = dgvData.Columns[ConfigHelper.LanguageConfig.langDgvColumnCarrierType.strColumnNameName].Index;
            return "[" + dgvData.SelectedRows[0].Cells[intIndexSingleWord].Value.ToString() + "] " + dgvData.SelectedRows[0].Cells[intIndexName].Value.ToString();
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count == 0)
            {
                MessageBox.Show(new ErrorManip.ErrorData(ConstantDict.CONSTANT_ERROR_NO_ROW_SELECTED).ToString());
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// 双击表格时视同点了确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvData_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

    }
}
