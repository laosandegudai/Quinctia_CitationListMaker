﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.FunctionForms
{
    public partial class FormPatentDocument : Form
    {
        public FormPatentDocument()
        {
            InitializeComponent();
        }

        public FormPatentDocument(RefPatentDocument refPatentDocument)
        {
            InitializeComponent();
            SetTxtControls(refPatentDocument);
        }

        private void SetTxtControls(RefPatentDocument refPatentDocument)
        {
            this.txtOwner.Text = refPatentDocument.strOwner.Trim();
            this.txtTitle.Text = refPatentDocument.strTitle.Trim();
            this.txtCountry.Text = refPatentDocument.strCountry.Trim();
            this.txtPN.Text = refPatentDocument.strPN.Trim();
            this.cmbLiteratureType.Text = refPatentDocument.strLiteratureType;
            this.cmbLiteratureCarrier.Text = refPatentDocument.strLiteratureCarrier;
            this.txtOpenTime.Text = refPatentDocument.strOpenTime.Trim();
            this.txtRefTime.Text = refPatentDocument.strRefTime.Trim();
            this.txtPath.Text = refPatentDocument.strPath.Trim();
        }

        /// <summary>
        /// 时钟，因为在ComboBox的SelectedIndexChanged事件中无法再次
        /// 改变ComboBox的值，因此需要通过这个时钟在10毫秒后改变
        /// </summary>
        Timer tmrRefreshComboBox = new Timer();

        /// <summary>
        /// 控件cmbLiteratureType的刷新用值
        /// </summary>
        string strLiteratureType = "[M] 专著";
        /// <summary>
        /// 控件cmbLiteratureCarrier的刷新用值
        /// </summary>
        string strLiteratureCarrier = "[  ] 不填写";

        private void FormPatentDocument_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetControlData();
            SetComboBoxTimer();
        }

        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strTitle;
            this.txtIntroduction.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strIntroduction.Replace("|", "\r\n"); ;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strCancel;

            this.lblOwner.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strOwner.Trim();
            this.lblTitle.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strXTitle.Trim();
            this.lblCountry.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strCountry.Trim();
            this.lblPN.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strPN.Trim();
            this.lblLiteratureType.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strLiteratureType.Trim();
            this.lblOpenTime.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strOpenTime.Trim();
            this.lblRefTime.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strRefTime.Trim();
            this.lblPath.Text = ConfigHelper.LanguageConfig.langFormPatentDocument.strPath.Trim();
        }

        private void SetControlData()
        {
            cmbLiteratureType.Items.Add("[M] 专著");
            cmbLiteratureType.Items.Add("[J] 期刊");
            cmbLiteratureType.Items.Add("[D] 学位论文");
            cmbLiteratureType.Items.Add("[.] 更多类型");
            cmbLiteratureType.Text = cmbLiteratureType.Items[0].ToString();

            cmbLiteratureCarrier.Items.Add("[  ] 不填写");
            cmbLiteratureCarrier.Items.Add("[CD] 光盘");
            cmbLiteratureCarrier.Items.Add("[OL] 联机网络");
            cmbLiteratureCarrier.Items.Add("[..] 更多载体");
            cmbLiteratureCarrier.Text = cmbLiteratureCarrier.Items[0].ToString();
        }

        /// <summary>
        /// 设置时钟 tmrRefreshComboBox
        /// </summary>
        private void SetComboBoxTimer()
        {
            this.tmrRefreshComboBox.Enabled = true;
            this.tmrRefreshComboBox.Interval = 10;
            this.tmrRefreshComboBox.Tick += (obj, arg) =>
            {
                this.cmbLiteratureType.Text = strLiteratureType;
                this.cmbLiteratureCarrier.Text = strLiteratureCarrier;
                this.tmrRefreshComboBox.Stop();
            };
        }


        public RefPatentDocument refPatentDocument = null;

        private void SetRefPatentDocument()
        {
            refPatentDocument = new RefPatentDocument();
            refPatentDocument.strRefType = ConstantDict.CONSTANT_REF_TYPE_PATENT_DOCUMENT;
            refPatentDocument.strId = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            refPatentDocument.strOwner = this.txtOwner.Text.Trim();
            refPatentDocument.strTitle = this.txtTitle.Text.Trim();
            refPatentDocument.strCountry = this.txtCountry.Text.Trim();
            refPatentDocument.strPN = this.txtPN.Text.Trim();
            refPatentDocument.strLiteratureType = this.cmbLiteratureType.Text.Trim();
            refPatentDocument.strLiteratureCarrier = this.cmbLiteratureCarrier.Text.Trim();
            refPatentDocument.strOpenTime = this.txtOpenTime.Text.Trim();
            refPatentDocument.strRefTime = this.txtRefTime.Text.Trim();
            refPatentDocument.strPath = this.txtPath.Text.Trim();
        }

        private bool ValidityCheck(out string errorcode)
        {
            errorcode = "";

            if (string.IsNullOrWhiteSpace(txtOwner.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_OWNER;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_TITLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtCountry.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_COUNTRY;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtPN.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_PN;
                return false;
            }

            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorcode = "";
            if (ValidityCheck(out errorcode))
            {
                SetRefPatentDocument();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(new ErrorManip.ErrorData(errorcode).ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLiteratureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多类型时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureType.Text == "[.] 更多类型")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureTypePath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_TYPE]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureType = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureType = this.cmbLiteratureType.Text;
            }
        }

        private void cmbLiteratureCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多载体时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureCarrier.Text == "[..] 更多载体")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureCarrierPath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_CARRIER]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureCarrier = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            }
        }

        private void cmbLiteratureType_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }

        private void cmbLiteratureCarrier_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }
    }
}
