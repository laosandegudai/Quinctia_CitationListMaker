﻿namespace Quinctia.FunctionForms
{
    partial class FormMonographContribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMonographContribution));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbLiteratureCarrier = new System.Windows.Forms.ComboBox();
            this.cmbLiteratureType = new System.Windows.Forms.ComboBox();
            this.lblPath = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.lblRefTime = new System.Windows.Forms.Label();
            this.txtRefTime = new System.Windows.Forms.TextBox();
            this.lblRefPage = new System.Windows.Forms.Label();
            this.txtRefPage = new System.Windows.Forms.TextBox();
            this.lblPubYear = new System.Windows.Forms.Label();
            this.txtPubYear = new System.Windows.Forms.TextBox();
            this.lblPubPeople = new System.Windows.Forms.Label();
            this.txtPubPeople = new System.Windows.Forms.TextBox();
            this.txtPubPlace = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.lblPubPlace = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblOtherTitleInfo = new System.Windows.Forms.Label();
            this.txtOtherTitleInfo = new System.Windows.Forms.TextBox();
            this.lblMonoTitle = new System.Windows.Forms.Label();
            this.txtMonoTitle = new System.Windows.Forms.TextBox();
            this.lblMonoMainPeople = new System.Windows.Forms.Label();
            this.txtMonoMainPeople = new System.Windows.Forms.TextBox();
            this.lblOtherPeople = new System.Windows.Forms.Label();
            this.txtOtherPeople = new System.Windows.Forms.TextBox();
            this.lblLiteratureType = new System.Windows.Forms.Label();
            this.txtContributionTitle = new System.Windows.Forms.TextBox();
            this.lblContributionTitle = new System.Windows.Forms.Label();
            this.txtMainPeople = new System.Windows.Forms.TextBox();
            this.lblMainPeople = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtIntroduction = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 387);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 75);
            this.panel1.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(448, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(124, 55);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "button2";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(318, 8);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(124, 55);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "button1";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(584, 387);
            this.panel2.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.cmbLiteratureCarrier);
            this.panel4.Controls.Add(this.cmbLiteratureType);
            this.panel4.Controls.Add(this.lblPath);
            this.panel4.Controls.Add(this.txtPath);
            this.panel4.Controls.Add(this.lblRefTime);
            this.panel4.Controls.Add(this.txtRefTime);
            this.panel4.Controls.Add(this.lblRefPage);
            this.panel4.Controls.Add(this.txtRefPage);
            this.panel4.Controls.Add(this.lblPubYear);
            this.panel4.Controls.Add(this.txtPubYear);
            this.panel4.Controls.Add(this.lblPubPeople);
            this.panel4.Controls.Add(this.txtPubPeople);
            this.panel4.Controls.Add(this.txtPubPlace);
            this.panel4.Controls.Add(this.txtVersion);
            this.panel4.Controls.Add(this.lblPubPlace);
            this.panel4.Controls.Add(this.lblVersion);
            this.panel4.Controls.Add(this.lblOtherTitleInfo);
            this.panel4.Controls.Add(this.txtOtherTitleInfo);
            this.panel4.Controls.Add(this.lblMonoTitle);
            this.panel4.Controls.Add(this.txtMonoTitle);
            this.panel4.Controls.Add(this.lblMonoMainPeople);
            this.panel4.Controls.Add(this.txtMonoMainPeople);
            this.panel4.Controls.Add(this.lblOtherPeople);
            this.panel4.Controls.Add(this.txtOtherPeople);
            this.panel4.Controls.Add(this.lblLiteratureType);
            this.panel4.Controls.Add(this.txtContributionTitle);
            this.panel4.Controls.Add(this.lblContributionTitle);
            this.panel4.Controls.Add(this.txtMainPeople);
            this.panel4.Controls.Add(this.lblMainPeople);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(410, 387);
            this.panel4.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(3, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 16);
            this.label5.TabIndex = 57;
            this.label5.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(3, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 16);
            this.label4.TabIndex = 56;
            this.label4.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(3, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 55;
            this.label2.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 16);
            this.label1.TabIndex = 54;
            this.label1.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(3, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "*";
            // 
            // cmbLiteratureCarrier
            // 
            this.cmbLiteratureCarrier.FormattingEnabled = true;
            this.cmbLiteratureCarrier.Location = new System.Drawing.Point(276, 64);
            this.cmbLiteratureCarrier.Name = "cmbLiteratureCarrier";
            this.cmbLiteratureCarrier.Size = new System.Drawing.Size(115, 20);
            this.cmbLiteratureCarrier.TabIndex = 3;
            this.cmbLiteratureCarrier.SelectedIndexChanged += new System.EventHandler(this.cmbLiteratureCarrier_SelectedIndexChanged);
            this.cmbLiteratureCarrier.TextUpdate += new System.EventHandler(this.cmbLiteratureCarrier_TextUpdate);
            // 
            // cmbLiteratureType
            // 
            this.cmbLiteratureType.FormattingEnabled = true;
            this.cmbLiteratureType.Location = new System.Drawing.Point(148, 64);
            this.cmbLiteratureType.Name = "cmbLiteratureType";
            this.cmbLiteratureType.Size = new System.Drawing.Size(115, 20);
            this.cmbLiteratureType.TabIndex = 2;
            this.cmbLiteratureType.SelectedIndexChanged += new System.EventHandler(this.cmbLiteratureType_SelectedIndexChanged);
            this.cmbLiteratureType.TextUpdate += new System.EventHandler(this.cmbLiteratureType_TextUpdate);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(17, 360);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(101, 12);
            this.lblPath.TabIndex = 53;
            this.lblPath.Text = "获取和访问路径：";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(148, 357);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(243, 21);
            this.txtPath.TabIndex = 14;
            // 
            // lblRefTime
            // 
            this.lblRefTime.AutoSize = true;
            this.lblRefTime.Location = new System.Drawing.Point(17, 333);
            this.lblRefTime.Name = "lblRefTime";
            this.lblRefTime.Size = new System.Drawing.Size(65, 12);
            this.lblRefTime.TabIndex = 51;
            this.lblRefTime.Text = "引用日期：";
            // 
            // txtRefTime
            // 
            this.txtRefTime.Location = new System.Drawing.Point(148, 330);
            this.txtRefTime.Name = "txtRefTime";
            this.txtRefTime.Size = new System.Drawing.Size(243, 21);
            this.txtRefTime.TabIndex = 13;
            // 
            // lblRefPage
            // 
            this.lblRefPage.AutoSize = true;
            this.lblRefPage.Location = new System.Drawing.Point(17, 306);
            this.lblRefPage.Name = "lblRefPage";
            this.lblRefPage.Size = new System.Drawing.Size(101, 12);
            this.lblRefPage.TabIndex = 49;
            this.lblRefPage.Text = "析出文献的页码：";
            // 
            // txtRefPage
            // 
            this.txtRefPage.Location = new System.Drawing.Point(148, 303);
            this.txtRefPage.Name = "txtRefPage";
            this.txtRefPage.Size = new System.Drawing.Size(243, 21);
            this.txtRefPage.TabIndex = 12;
            // 
            // lblPubYear
            // 
            this.lblPubYear.AutoSize = true;
            this.lblPubYear.Location = new System.Drawing.Point(17, 279);
            this.lblPubYear.Name = "lblPubYear";
            this.lblPubYear.Size = new System.Drawing.Size(53, 12);
            this.lblPubYear.TabIndex = 47;
            this.lblPubYear.Text = "出版年：";
            // 
            // txtPubYear
            // 
            this.txtPubYear.Location = new System.Drawing.Point(148, 276);
            this.txtPubYear.Name = "txtPubYear";
            this.txtPubYear.Size = new System.Drawing.Size(243, 21);
            this.txtPubYear.TabIndex = 11;
            // 
            // lblPubPeople
            // 
            this.lblPubPeople.AutoSize = true;
            this.lblPubPeople.Location = new System.Drawing.Point(17, 252);
            this.lblPubPeople.Name = "lblPubPeople";
            this.lblPubPeople.Size = new System.Drawing.Size(53, 12);
            this.lblPubPeople.TabIndex = 45;
            this.lblPubPeople.Text = "出版者：";
            // 
            // txtPubPeople
            // 
            this.txtPubPeople.Location = new System.Drawing.Point(148, 249);
            this.txtPubPeople.Name = "txtPubPeople";
            this.txtPubPeople.Size = new System.Drawing.Size(243, 21);
            this.txtPubPeople.TabIndex = 10;
            // 
            // txtPubPlace
            // 
            this.txtPubPlace.Location = new System.Drawing.Point(148, 222);
            this.txtPubPlace.Name = "txtPubPlace";
            this.txtPubPlace.Size = new System.Drawing.Size(243, 21);
            this.txtPubPlace.TabIndex = 9;
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(148, 198);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(243, 21);
            this.txtVersion.TabIndex = 8;
            // 
            // lblPubPlace
            // 
            this.lblPubPlace.AutoSize = true;
            this.lblPubPlace.Location = new System.Drawing.Point(17, 225);
            this.lblPubPlace.Name = "lblPubPlace";
            this.lblPubPlace.Size = new System.Drawing.Size(53, 12);
            this.lblPubPlace.TabIndex = 41;
            this.lblPubPlace.Text = "出版地：";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(17, 201);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(53, 12);
            this.lblVersion.TabIndex = 40;
            this.lblVersion.Text = "版本项：";
            // 
            // lblOtherTitleInfo
            // 
            this.lblOtherTitleInfo.AutoSize = true;
            this.lblOtherTitleInfo.Location = new System.Drawing.Point(17, 176);
            this.lblOtherTitleInfo.Name = "lblOtherTitleInfo";
            this.lblOtherTitleInfo.Size = new System.Drawing.Size(89, 12);
            this.lblOtherTitleInfo.TabIndex = 39;
            this.lblOtherTitleInfo.Text = "其他题名信息：";
            // 
            // txtOtherTitleInfo
            // 
            this.txtOtherTitleInfo.Location = new System.Drawing.Point(148, 173);
            this.txtOtherTitleInfo.Name = "txtOtherTitleInfo";
            this.txtOtherTitleInfo.Size = new System.Drawing.Size(243, 21);
            this.txtOtherTitleInfo.TabIndex = 7;
            // 
            // lblMonoTitle
            // 
            this.lblMonoTitle.AutoSize = true;
            this.lblMonoTitle.Location = new System.Drawing.Point(17, 149);
            this.lblMonoTitle.Name = "lblMonoTitle";
            this.lblMonoTitle.Size = new System.Drawing.Size(65, 12);
            this.lblMonoTitle.TabIndex = 37;
            this.lblMonoTitle.Text = "专著题名：";
            // 
            // txtMonoTitle
            // 
            this.txtMonoTitle.Location = new System.Drawing.Point(148, 146);
            this.txtMonoTitle.Name = "txtMonoTitle";
            this.txtMonoTitle.Size = new System.Drawing.Size(243, 21);
            this.txtMonoTitle.TabIndex = 6;
            // 
            // lblMonoMainPeople
            // 
            this.lblMonoMainPeople.AutoSize = true;
            this.lblMonoMainPeople.Location = new System.Drawing.Point(17, 121);
            this.lblMonoMainPeople.Name = "lblMonoMainPeople";
            this.lblMonoMainPeople.Size = new System.Drawing.Size(101, 12);
            this.lblMonoMainPeople.TabIndex = 35;
            this.lblMonoMainPeople.Text = "专著主要责任者：";
            // 
            // txtMonoMainPeople
            // 
            this.txtMonoMainPeople.Location = new System.Drawing.Point(148, 118);
            this.txtMonoMainPeople.Name = "txtMonoMainPeople";
            this.txtMonoMainPeople.Size = new System.Drawing.Size(243, 21);
            this.txtMonoMainPeople.TabIndex = 5;
            // 
            // lblOtherPeople
            // 
            this.lblOtherPeople.AutoSize = true;
            this.lblOtherPeople.Location = new System.Drawing.Point(17, 95);
            this.lblOtherPeople.Name = "lblOtherPeople";
            this.lblOtherPeople.Size = new System.Drawing.Size(125, 12);
            this.lblOtherPeople.TabIndex = 32;
            this.lblOtherPeople.Text = "析出文献其他责任者：";
            // 
            // txtOtherPeople
            // 
            this.txtOtherPeople.Location = new System.Drawing.Point(148, 90);
            this.txtOtherPeople.Name = "txtOtherPeople";
            this.txtOtherPeople.Size = new System.Drawing.Size(243, 21);
            this.txtOtherPeople.TabIndex = 4;
            // 
            // lblLiteratureType
            // 
            this.lblLiteratureType.AutoSize = true;
            this.lblLiteratureType.Location = new System.Drawing.Point(17, 67);
            this.lblLiteratureType.Name = "lblLiteratureType";
            this.lblLiteratureType.Size = new System.Drawing.Size(89, 12);
            this.lblLiteratureType.TabIndex = 30;
            this.lblLiteratureType.Text = "文献类型标志：";
            // 
            // txtContributionTitle
            // 
            this.txtContributionTitle.Location = new System.Drawing.Point(148, 36);
            this.txtContributionTitle.Name = "txtContributionTitle";
            this.txtContributionTitle.Size = new System.Drawing.Size(243, 21);
            this.txtContributionTitle.TabIndex = 1;
            // 
            // lblContributionTitle
            // 
            this.lblContributionTitle.AutoSize = true;
            this.lblContributionTitle.Location = new System.Drawing.Point(17, 39);
            this.lblContributionTitle.Name = "lblContributionTitle";
            this.lblContributionTitle.Size = new System.Drawing.Size(89, 12);
            this.lblContributionTitle.TabIndex = 28;
            this.lblContributionTitle.Text = "析出文献题名：";
            // 
            // txtMainPeople
            // 
            this.txtMainPeople.Location = new System.Drawing.Point(148, 9);
            this.txtMainPeople.Name = "txtMainPeople";
            this.txtMainPeople.Size = new System.Drawing.Size(243, 21);
            this.txtMainPeople.TabIndex = 0;
            // 
            // lblMainPeople
            // 
            this.lblMainPeople.AutoSize = true;
            this.lblMainPeople.Location = new System.Drawing.Point(17, 12);
            this.lblMainPeople.Name = "lblMainPeople";
            this.lblMainPeople.Size = new System.Drawing.Size(125, 12);
            this.lblMainPeople.TabIndex = 26;
            this.lblMainPeople.Text = "析出文献主要责任者：";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtIntroduction);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(410, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(174, 387);
            this.panel3.TabIndex = 0;
            // 
            // txtIntroduction
            // 
            this.txtIntroduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntroduction.Enabled = false;
            this.txtIntroduction.Location = new System.Drawing.Point(0, 0);
            this.txtIntroduction.Multiline = true;
            this.txtIntroduction.Name = "txtIntroduction";
            this.txtIntroduction.Size = new System.Drawing.Size(174, 387);
            this.txtIntroduction.TabIndex = 0;
            // 
            // FormMonographContribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMonographContribution";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMonographContribution";
            this.Load += new System.EventHandler(this.FormMonographContribution_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtIntroduction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblRefPage;
        private System.Windows.Forms.TextBox txtRefPage;
        private System.Windows.Forms.Label lblPubYear;
        private System.Windows.Forms.TextBox txtPubYear;
        private System.Windows.Forms.Label lblPubPeople;
        private System.Windows.Forms.TextBox txtPubPeople;
        private System.Windows.Forms.TextBox txtPubPlace;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label lblPubPlace;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblOtherTitleInfo;
        private System.Windows.Forms.TextBox txtOtherTitleInfo;
        private System.Windows.Forms.Label lblMonoTitle;
        private System.Windows.Forms.TextBox txtMonoTitle;
        private System.Windows.Forms.Label lblMonoMainPeople;
        private System.Windows.Forms.TextBox txtMonoMainPeople;
        private System.Windows.Forms.Label lblOtherPeople;
        private System.Windows.Forms.TextBox txtOtherPeople;
        private System.Windows.Forms.Label lblLiteratureType;
        private System.Windows.Forms.TextBox txtContributionTitle;
        private System.Windows.Forms.Label lblContributionTitle;
        private System.Windows.Forms.TextBox txtMainPeople;
        private System.Windows.Forms.Label lblMainPeople;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label lblRefTime;
        private System.Windows.Forms.TextBox txtRefTime;
        private System.Windows.Forms.ComboBox cmbLiteratureCarrier;
        private System.Windows.Forms.ComboBox cmbLiteratureType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}