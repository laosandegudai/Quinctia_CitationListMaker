﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Quinctia.FunctionForms
{
    public partial class FormMonograph : Form
    {
        public FormMonograph()
        {
            InitializeComponent();
        }

        public FormMonograph(RefMonograph refMonograph)
        {
            InitializeComponent();
            SetTxtControls(refMonograph);
        }

        /// <summary>
        /// 根据输入给文本控件赋值
        /// </summary>
        /// <param name="refMonograph"></param>
        private void SetTxtControls(RefMonograph refMonograph)
        {
            this.txtMainPeople.Text = refMonograph.strMainPeople;
            this.txtTitle.Text = refMonograph.strTitle;
            this.txtTitleTip.Text = refMonograph.strTitleTip;
            this.cmbLiteratureType.Text = refMonograph.strLiteratureType;
            this.cmbLiteratureCarrier.Text = refMonograph.strLiteratureCarrier;
            this.txtOtherPeople.Text = refMonograph.strOtherPeople;
            this.txtVersion.Text = refMonograph.strVersion;
            this.txtPubPlace.Text = refMonograph.strPubPlace;
            this.txtPubPeople.Text = refMonograph.strPubPeople;
            this.txtPubYear.Text = refMonograph.strPubYear;
            this.txtRefPage.Text = refMonograph.strRefPage;
            this.txtRefTime.Text = refMonograph.strRefTime;
            this.txtPath.Text = refMonograph.strPath;
        }

        /// <summary>
        /// 时钟，因为在ComboBox的SelectedIndexChanged事件中无法再次
        /// 改变ComboBox的值，因此需要通过这个时钟在10毫秒后改变
        /// </summary>
        Timer tmrRefreshComboBox = new Timer();

        /// <summary>
        /// 控件cmbLiteratureType的刷新用值
        /// </summary>
        string strLiteratureType = "[M] 专著";
        /// <summary>
        /// 控件cmbLiteratureCarrier的刷新用值
        /// </summary>
        string strLiteratureCarrier = "[  ] 不填写";

        private void FormMonograph_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetControlData();
            SetComboBoxTimer();
        }

        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormMonograph.strTitle;
            this.txtIntroduction.Text = ConfigHelper.LanguageConfig.langFormMonograph.strIntroduction.Replace("|", "\r\n"); ;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormMonograph.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormMonograph.strCancel;

            this.lblMainPeople.Text = ConfigHelper.LanguageConfig.langFormMonograph.strMainPeople.Trim();
            this.lblTitle.Text = ConfigHelper.LanguageConfig.langFormMonograph.strXTitle.Trim();
            this.lblTitleTip.Text = ConfigHelper.LanguageConfig.langFormMonograph.strTitleTip.Trim();
            this.lblLiteratureType.Text = ConfigHelper.LanguageConfig.langFormMonograph.strLiteratureType.Trim();
            this.lblOtherPeople.Text = ConfigHelper.LanguageConfig.langFormMonograph.strOtherPeople.Trim();
            this.lblVersion.Text = ConfigHelper.LanguageConfig.langFormMonograph.strVersion.Trim();
            this.lblPubPlace.Text = ConfigHelper.LanguageConfig.langFormMonograph.strPubPlace.Trim();
            this.lblPubPeople.Text = ConfigHelper.LanguageConfig.langFormMonograph.strPubPeople.Trim();
            this.lblPubYear.Text = ConfigHelper.LanguageConfig.langFormMonograph.strPubYear.Trim();
            this.lblRefPage.Text = ConfigHelper.LanguageConfig.langFormMonograph.strRefPage.Trim();
            this.lblRefTime.Text = ConfigHelper.LanguageConfig.langFormMonograph.strRefTime.Trim();
            this.lblPath.Text = ConfigHelper.LanguageConfig.langFormMonograph.strPath.Trim();
        }

        /// <summary>
        /// 设置控件上显示数据
        /// </summary>
        private void SetControlData()
        {
            cmbLiteratureType.Items.Add("[M] 专著");
            cmbLiteratureType.Items.Add("[J] 期刊");
            cmbLiteratureType.Items.Add("[D] 学位论文");
            cmbLiteratureType.Items.Add("[.] 更多类型");
            cmbLiteratureType.Text = strLiteratureType;

            cmbLiteratureCarrier.Items.Add("[  ] 不填写");
            cmbLiteratureCarrier.Items.Add("[CD] 光盘");
            cmbLiteratureCarrier.Items.Add("[OL] 联机网络");
            cmbLiteratureCarrier.Items.Add("[..] 更多载体");
            cmbLiteratureCarrier.Text = strLiteratureCarrier;
        }

        /// <summary>
        /// 设置时钟 tmrRefreshComboBox
        /// </summary>
        private void SetComboBoxTimer()
        {
            this.tmrRefreshComboBox.Enabled = true;
            this.tmrRefreshComboBox.Interval = 10;
            this.tmrRefreshComboBox.Tick += (obj, arg) =>
            {
                this.cmbLiteratureType.Text = strLiteratureType;
                this.cmbLiteratureCarrier.Text = strLiteratureCarrier;
                this.tmrRefreshComboBox.Stop();
            };
        }

        public RefMonograph refMonograph = null;

        private void SetRefMonograph()
        {
            refMonograph = new RefMonograph();
            refMonograph.strRefType = ConstantDict.CONSTANT_REF_TYPE_MONOGRAPH;
            refMonograph.strId = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            refMonograph.strMainPeople = this.txtMainPeople.Text.Trim();
            refMonograph.strTitle = this.txtTitle.Text.Trim();
            refMonograph.strTitleTip = this.txtTitleTip.Text.Trim();
            refMonograph.strLiteratureType = this.cmbLiteratureType.Text.Trim();
            refMonograph.strLiteratureCarrier = this.cmbLiteratureCarrier.Text.Trim();
            refMonograph.strOtherPeople = this.txtOtherPeople.Text.Trim();
            refMonograph.strVersion = this.txtVersion.Text.Trim();
            refMonograph.strPubPlace = this.txtPubPlace.Text.Trim();
            refMonograph.strPubPeople = this.txtPubPeople.Text.Trim();
            refMonograph.strPubYear = this.txtPubYear.Text.Trim();
            refMonograph.strRefPage = this.txtRefPage.Text.Trim();
            refMonograph.strRefTime = this.txtRefTime.Text.Trim();
            refMonograph.strPath = this.txtPath.Text.Trim();
        }


        private bool ValidityCheck(out string errorcode)
        {
            errorcode = "";

            if (string.IsNullOrWhiteSpace(txtMainPeople.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_MONOGRAPH_NEED_MAIN_PEOPLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_MONOGRAPH_NEED_TITLE;
                return false;
            }

            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorcode = "";
            if (ValidityCheck(out errorcode))
            {
                SetRefMonograph();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(new ErrorManip.ErrorData(errorcode).ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLiteratureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多类型时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureType.Text == "[.] 更多类型")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureTypePath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_TYPE]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureType = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureType = this.cmbLiteratureType.Text;
            }
        }

        private void cmbLiteratureCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多载体时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureCarrier.Text == "[..] 更多载体")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureCarrierPath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_CARRIER]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureCarrier = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            }
        }

        /// <summary>
        /// 禁止手动修改ComboBox的值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLiteratureType_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }

        /// <summary>
        /// 禁止手动修改ComboBox的值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLiteratureCarrier_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }
    }
}
