﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.FunctionForms
{
    public partial class FormElectronicDocument : Form
    {
        public FormElectronicDocument()
        {
            InitializeComponent();
        }

        public FormElectronicDocument(RefElectronicDocument refElectronicDocument)
        {
            InitializeComponent();
            SetTxtControls(refElectronicDocument);
        }

        /// <summary>
        /// 根据输入给文本控件赋值
        /// </summary>
        /// <param name="refElectronicDocument"></param>
        private void SetTxtControls(RefElectronicDocument refElectronicDocument)
        {
            this.txtMainPeople.Text = refElectronicDocument.strMainPeople;
            this.txtTitle.Text = refElectronicDocument.strTitle;
            this.txtTitleTip.Text = refElectronicDocument.strTitleTip;
            this.cmbLiteratureType.Text = refElectronicDocument.strLiteratureType;
            this.cmbLiteratureCarrier.Text = refElectronicDocument.strLiteratureCarrier;
            this.txtPubPlace.Text = refElectronicDocument.strPubPlace;
            this.txtPubPeople.Text = refElectronicDocument.strPubPeople;
            this.txtPubYear.Text = refElectronicDocument.strPubYear;
            this.txtLastChange.Text = refElectronicDocument.strLastChange;
            this.txtRefTime.Text = refElectronicDocument.strRefTime;
            this.txtPath.Text = refElectronicDocument.strPath;
        }

        /// <summary>
        /// 时钟，因为在ComboBox的SelectedIndexChanged事件中无法再次
        /// 改变ComboBox的值，因此需要通过这个时钟在10毫秒后改变
        /// </summary>
        Timer tmrRefreshComboBox = new Timer();

        /// <summary>
        /// 控件cmbLiteratureType的刷新用值
        /// </summary>
        string strLiteratureType = "[M] 专著";
        /// <summary>
        /// 控件cmbLiteratureCarrier的刷新用值
        /// </summary>
        string strLiteratureCarrier = "[  ] 不填写";

        private void FormElectronicDocument_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetControlData();
            SetComboBoxTimer();
        }

        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strTitle;
            this.txtIntroduction.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strIntroduction.Replace("|", "\r\n"); ;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strCancel;

            this.lblMainPeople.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strMainPeople.Trim();
            this.lblTitle.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strXTitle.Trim();
            this.lblTitleTip.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strTitleTip.Trim();
            this.lblLiteratureType.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strLiteratureType.Trim();
            this.lblPubPlace.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strPubPlace.Trim();
            this.lblPubPeople.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strPubPeople.Trim();
            this.lblPubYear.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strPubYear.Trim();
            this.lblLastChange.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strLastChange.Trim();
            this.lblRefTime.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strRefTime.Trim();
            this.lblPath.Text = ConfigHelper.LanguageConfig.langFormElectronicDocument.strPath.Trim();
        }

        private void SetControlData()
        {
            cmbLiteratureType.Items.Add("[M] 专著");
            cmbLiteratureType.Items.Add("[J] 期刊");
            cmbLiteratureType.Items.Add("[D] 学位论文");
            cmbLiteratureType.Items.Add("[.] 更多类型");
            cmbLiteratureType.Text = cmbLiteratureType.Items[0].ToString();

            cmbLiteratureCarrier.Items.Add("[  ] 不填写");
            cmbLiteratureCarrier.Items.Add("[CD] 光盘");
            cmbLiteratureCarrier.Items.Add("[OL] 联机网络");
            cmbLiteratureCarrier.Items.Add("[..] 更多载体");
            cmbLiteratureCarrier.Text = cmbLiteratureCarrier.Items[0].ToString();
        }

        /// <summary>
        /// 设置时钟 tmrRefreshComboBox
        /// </summary>
        private void SetComboBoxTimer()
        {
            this.tmrRefreshComboBox.Enabled = true;
            this.tmrRefreshComboBox.Interval = 10;
            this.tmrRefreshComboBox.Tick += (obj, arg) =>
            {
                this.cmbLiteratureType.Text = strLiteratureType;
                this.cmbLiteratureCarrier.Text = strLiteratureCarrier;
                this.tmrRefreshComboBox.Stop();
            };
        }

        public RefElectronicDocument refElectronicDocument = null;
        
        private void SetRefElectronicDocument()
        {
            refElectronicDocument = new RefElectronicDocument();
            refElectronicDocument.strRefType = ConstantDict.CONSTANT_REF_TYPE_ELECTRONIC_DOCUMENT;
            refElectronicDocument.strId = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            refElectronicDocument.strMainPeople = this.txtMainPeople.Text.Trim();
            refElectronicDocument.strTitle = this.txtTitle.Text.Trim();
            refElectronicDocument.strTitleTip = this.txtTitleTip.Text;
            refElectronicDocument.strLiteratureType = this.cmbLiteratureType.Text.Trim();
            refElectronicDocument.strLiteratureCarrier = this.cmbLiteratureCarrier.Text.Trim();
            refElectronicDocument.strPubPlace = this.txtPubPlace.Text.Trim();
            refElectronicDocument.strPubPeople = this.txtPubPeople.Text.Trim();
            refElectronicDocument.strPubYear = this.txtPubYear.Text.Trim();
            refElectronicDocument.strLastChange = this.txtLastChange.Text.Trim();
            refElectronicDocument.strRefTime = this.txtRefTime.Text.Trim();
            refElectronicDocument.strPath = this.txtPath.Text.Trim();
        }

        private bool ValidityCheck(out string errorcode)
        {
            errorcode = "";

            if (string.IsNullOrWhiteSpace(txtMainPeople.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_MAIN_PEOPLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_TITLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtPath.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_PATH;
                return false;
            }

            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorcode = "";
            if (ValidityCheck(out errorcode))
            {
                SetRefElectronicDocument();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(new ErrorManip.ErrorData(errorcode).ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLiteratureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多类型时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureType.Text == "[.] 更多类型")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureTypePath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_TYPE]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureType = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureType = this.cmbLiteratureType.Text;
            }
        }

        private void cmbLiteratureCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多载体时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureCarrier.Text == "[..] 更多载体")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureCarrierPath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_CARRIER]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureCarrier = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            }
        }

        private void cmbLiteratureType_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }

        private void cmbLiteratureCarrier_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }
    }
}
