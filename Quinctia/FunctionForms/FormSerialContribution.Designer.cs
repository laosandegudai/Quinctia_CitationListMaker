﻿namespace Quinctia.FunctionForms
{
    partial class FormSerialContribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSerialContribution));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSerialInfo = new System.Windows.Forms.TextBox();
            this.lblSerialInfo = new System.Windows.Forms.Label();
            this.lblSerialName = new System.Windows.Forms.Label();
            this.txtSerialName = new System.Windows.Forms.TextBox();
            this.cmbLiteratureCarrier = new System.Windows.Forms.ComboBox();
            this.lblPath = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.txtRefTime = new System.Windows.Forms.TextBox();
            this.lblRefTime = new System.Windows.Forms.Label();
            this.lblPosition = new System.Windows.Forms.Label();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.cmbLiteratureType = new System.Windows.Forms.ComboBox();
            this.lblLiteratureType = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtMainPeople = new System.Windows.Forms.TextBox();
            this.lblMainPeople = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtIntroduction = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 387);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 75);
            this.panel1.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(448, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(124, 55);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "button2";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(318, 8);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(124, 55);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "button1";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(584, 387);
            this.panel2.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.txtSerialInfo);
            this.panel4.Controls.Add(this.lblSerialInfo);
            this.panel4.Controls.Add(this.lblSerialName);
            this.panel4.Controls.Add(this.txtSerialName);
            this.panel4.Controls.Add(this.cmbLiteratureCarrier);
            this.panel4.Controls.Add(this.lblPath);
            this.panel4.Controls.Add(this.txtPath);
            this.panel4.Controls.Add(this.txtRefTime);
            this.panel4.Controls.Add(this.lblRefTime);
            this.panel4.Controls.Add(this.lblPosition);
            this.panel4.Controls.Add(this.txtPosition);
            this.panel4.Controls.Add(this.cmbLiteratureType);
            this.panel4.Controls.Add(this.lblLiteratureType);
            this.panel4.Controls.Add(this.txtTitle);
            this.panel4.Controls.Add(this.lblTitle);
            this.panel4.Controls.Add(this.txtMainPeople);
            this.panel4.Controls.Add(this.lblMainPeople);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(410, 387);
            this.panel4.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(3, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 16);
            this.label4.TabIndex = 94;
            this.label4.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(3, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 93;
            this.label2.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(3, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 16);
            this.label1.TabIndex = 92;
            this.label1.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(3, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "*";
            // 
            // txtSerialInfo
            // 
            this.txtSerialInfo.Location = new System.Drawing.Point(151, 196);
            this.txtSerialInfo.Name = "txtSerialInfo";
            this.txtSerialInfo.Size = new System.Drawing.Size(243, 21);
            this.txtSerialInfo.TabIndex = 88;
            // 
            // lblSerialInfo
            // 
            this.lblSerialInfo.AutoSize = true;
            this.lblSerialInfo.Location = new System.Drawing.Point(20, 199);
            this.lblSerialInfo.Name = "lblSerialInfo";
            this.lblSerialInfo.Size = new System.Drawing.Size(89, 12);
            this.lblSerialInfo.TabIndex = 91;
            this.lblSerialInfo.Text = "其他题名信息：";
            // 
            // lblSerialName
            // 
            this.lblSerialName.AutoSize = true;
            this.lblSerialName.Location = new System.Drawing.Point(20, 170);
            this.lblSerialName.Name = "lblSerialName";
            this.lblSerialName.Size = new System.Drawing.Size(101, 12);
            this.lblSerialName.TabIndex = 90;
            this.lblSerialName.Text = "连续出版物题名：";
            // 
            // txtSerialName
            // 
            this.txtSerialName.Location = new System.Drawing.Point(151, 167);
            this.txtSerialName.Name = "txtSerialName";
            this.txtSerialName.Size = new System.Drawing.Size(243, 21);
            this.txtSerialName.TabIndex = 87;
            // 
            // cmbLiteratureCarrier
            // 
            this.cmbLiteratureCarrier.FormattingEnabled = true;
            this.cmbLiteratureCarrier.Location = new System.Drawing.Point(279, 141);
            this.cmbLiteratureCarrier.Name = "cmbLiteratureCarrier";
            this.cmbLiteratureCarrier.Size = new System.Drawing.Size(115, 20);
            this.cmbLiteratureCarrier.TabIndex = 3;
            this.cmbLiteratureCarrier.SelectedIndexChanged += new System.EventHandler(this.cmbLiteratureCarrier_SelectedIndexChanged);
            this.cmbLiteratureCarrier.TextUpdate += new System.EventHandler(this.cmbLiteratureCarrier_TextUpdate);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(20, 284);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(101, 12);
            this.lblPath.TabIndex = 86;
            this.lblPath.Text = "获取和访问路径：";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(151, 281);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(243, 21);
            this.txtPath.TabIndex = 9;
            // 
            // txtRefTime
            // 
            this.txtRefTime.Location = new System.Drawing.Point(151, 254);
            this.txtRefTime.Name = "txtRefTime";
            this.txtRefTime.Size = new System.Drawing.Size(243, 21);
            this.txtRefTime.TabIndex = 8;
            // 
            // lblRefTime
            // 
            this.lblRefTime.AutoSize = true;
            this.lblRefTime.Location = new System.Drawing.Point(20, 257);
            this.lblRefTime.Name = "lblRefTime";
            this.lblRefTime.Size = new System.Drawing.Size(65, 12);
            this.lblRefTime.TabIndex = 82;
            this.lblRefTime.Text = "引用日期：";
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(11, 228);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(131, 12);
            this.lblPosition.TabIndex = 76;
            this.lblPosition.Text = "年,卷(期)-年,卷(期)：";
            // 
            // txtPosition
            // 
            this.txtPosition.Location = new System.Drawing.Point(151, 225);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Size = new System.Drawing.Size(243, 21);
            this.txtPosition.TabIndex = 4;
            // 
            // cmbLiteratureType
            // 
            this.cmbLiteratureType.FormattingEnabled = true;
            this.cmbLiteratureType.Location = new System.Drawing.Point(151, 141);
            this.cmbLiteratureType.Name = "cmbLiteratureType";
            this.cmbLiteratureType.Size = new System.Drawing.Size(115, 20);
            this.cmbLiteratureType.TabIndex = 2;
            this.cmbLiteratureType.SelectedIndexChanged += new System.EventHandler(this.cmbLiteratureType_SelectedIndexChanged);
            this.cmbLiteratureType.TextUpdate += new System.EventHandler(this.cmbLiteratureType_TextUpdate);
            // 
            // lblLiteratureType
            // 
            this.lblLiteratureType.AutoSize = true;
            this.lblLiteratureType.Location = new System.Drawing.Point(20, 144);
            this.lblLiteratureType.Name = "lblLiteratureType";
            this.lblLiteratureType.Size = new System.Drawing.Size(89, 12);
            this.lblLiteratureType.TabIndex = 71;
            this.lblLiteratureType.Text = "文献类型标志：";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(151, 113);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(243, 21);
            this.txtTitle.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(20, 116);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(89, 12);
            this.lblTitle.TabIndex = 69;
            this.lblTitle.Text = "析出文献题名：";
            // 
            // txtMainPeople
            // 
            this.txtMainPeople.Location = new System.Drawing.Point(151, 86);
            this.txtMainPeople.Name = "txtMainPeople";
            this.txtMainPeople.Size = new System.Drawing.Size(243, 21);
            this.txtMainPeople.TabIndex = 0;
            // 
            // lblMainPeople
            // 
            this.lblMainPeople.AutoSize = true;
            this.lblMainPeople.Location = new System.Drawing.Point(20, 89);
            this.lblMainPeople.Name = "lblMainPeople";
            this.lblMainPeople.Size = new System.Drawing.Size(125, 12);
            this.lblMainPeople.TabIndex = 67;
            this.lblMainPeople.Text = "析出文献主要责任者：";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtIntroduction);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(410, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(174, 387);
            this.panel3.TabIndex = 0;
            // 
            // txtIntroduction
            // 
            this.txtIntroduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntroduction.Enabled = false;
            this.txtIntroduction.Location = new System.Drawing.Point(0, 0);
            this.txtIntroduction.Multiline = true;
            this.txtIntroduction.Name = "txtIntroduction";
            this.txtIntroduction.Size = new System.Drawing.Size(174, 387);
            this.txtIntroduction.TabIndex = 0;
            // 
            // FormSerialContribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormSerialContribution";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSerialContribution";
            this.Load += new System.EventHandler(this.FormSerialContribution_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtIntroduction;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox cmbLiteratureCarrier;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.TextBox txtRefTime;
        private System.Windows.Forms.Label lblRefTime;
        private System.Windows.Forms.Label lblPosition;
        private System.Windows.Forms.TextBox txtPosition;
        private System.Windows.Forms.ComboBox cmbLiteratureType;
        private System.Windows.Forms.Label lblLiteratureType;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtMainPeople;
        private System.Windows.Forms.Label lblMainPeople;
        private System.Windows.Forms.TextBox txtSerialInfo;
        private System.Windows.Forms.Label lblSerialInfo;
        private System.Windows.Forms.Label lblSerialName;
        private System.Windows.Forms.TextBox txtSerialName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}