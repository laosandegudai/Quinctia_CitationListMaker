﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.FunctionForms
{
    public partial class FormSerial : Form
    {
        public FormSerial()
        {
            InitializeComponent();
        }

        public FormSerial(RefSerial refSerial)
        {
            InitializeComponent();
            SetTxtControls(refSerial);
        }

        private void SetTxtControls(RefSerial refSerial)
        {
            this.txtMainPeople.Text = refSerial.strMainPeople;
            this.txtTitle.Text = refSerial.strTitle;
            this.txtTitleTip.Text = refSerial.strTitleTip;
            this.cmbLiteratureType.Text = refSerial.strLiteratureType;
            this.cmbLiteratureCarrier.Text = refSerial.strLiteratureCarrier;
            this.txtPosition.Text = refSerial.strPosition;
            this.txtPubPlace.Text = refSerial.strPubPlace;
            this.txtPubPeople.Text = refSerial.strPubPeople;
            this.txtPubYear.Text = refSerial.strPubYear;
            this.txtRefTime.Text = refSerial.strRefTime;
            this.txtPath.Text = refSerial.strPath;
        }

        /// <summary>
        /// 时钟，因为在ComboBox的SelectedIndexChanged事件中无法再次
        /// 改变ComboBox的值，因此需要通过这个时钟在10毫秒后改变
        /// </summary>
        Timer tmrRefreshComboBox = new Timer();

        /// <summary>
        /// 控件cmbLiteratureType的刷新用值
        /// </summary>
        string strLiteratureType = "[M] 专著";
        /// <summary>
        /// 控件cmbLiteratureCarrier的刷新用值
        /// </summary>
        string strLiteratureCarrier = "[  ] 不填写";

        private void FormSerial_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetControlData();
            SetComboBoxTimer();
        }

        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormSerial.strTitle;
            this.txtIntroduction.Text = ConfigHelper.LanguageConfig.langFormSerial.strIntroduction.Replace("|", "\r\n"); ;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormSerial.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormSerial.strCancel;

            this.lblMainPeople.Text = ConfigHelper.LanguageConfig.langFormSerial.strMainPeople.Trim();
            this.lblTitle.Text = ConfigHelper.LanguageConfig.langFormSerial.strXTitle.Trim();
            this.lblTitleTip.Text = ConfigHelper.LanguageConfig.langFormSerial.strTitleTip.Trim();
            this.lblLiteratureType.Text = ConfigHelper.LanguageConfig.langFormSerial.strLiteratureType.Trim();
            this.lblPosition.Text = ConfigHelper.LanguageConfig.langFormSerial.strPosition.Trim();
            this.lblPubPlace.Text = ConfigHelper.LanguageConfig.langFormSerial.strPubPlace.Trim();
            this.lblPubPeople.Text = ConfigHelper.LanguageConfig.langFormSerial.strPubPeople.Trim();
            this.lblPubYear.Text = ConfigHelper.LanguageConfig.langFormSerial.strPubYear.Trim();
            this.lblRefTime.Text = ConfigHelper.LanguageConfig.langFormSerial.strRefTime.Trim();
            this.lblPath.Text = ConfigHelper.LanguageConfig.langFormSerial.strPath.Trim();
        }

        private void SetControlData()
        {
            cmbLiteratureType.Items.Add("[M] 专著");
            cmbLiteratureType.Items.Add("[J] 期刊");
            cmbLiteratureType.Items.Add("[D] 学位论文");
            cmbLiteratureType.Items.Add("[.] 更多类型");
            cmbLiteratureType.Text = cmbLiteratureType.Items[0].ToString();

            cmbLiteratureCarrier.Items.Add("[  ] 不填写");
            cmbLiteratureCarrier.Items.Add("[CD] 光盘");
            cmbLiteratureCarrier.Items.Add("[OL] 联机网络");
            cmbLiteratureCarrier.Items.Add("[..] 更多载体");
            cmbLiteratureCarrier.Text = cmbLiteratureCarrier.Items[0].ToString();
        }

        /// <summary>
        /// 设置时钟 tmrRefreshComboBox
        /// </summary>
        private void SetComboBoxTimer()
        {
            this.tmrRefreshComboBox.Enabled = true;
            this.tmrRefreshComboBox.Interval = 10;
            this.tmrRefreshComboBox.Tick += (obj, arg) =>
            {
                this.cmbLiteratureType.Text = strLiteratureType;
                this.cmbLiteratureCarrier.Text = strLiteratureCarrier;
                this.tmrRefreshComboBox.Stop();
            };
        }

        public RefSerial refSerial = null;
        
        private void SetRefSerial()
        {
            refSerial = new RefSerial();
            refSerial.strRefType = ConstantDict.CONSTANT_REF_TYPE_SERIAL;
            refSerial.strId = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            refSerial.strMainPeople = this.txtMainPeople.Text;
            refSerial.strTitle = this.txtTitle.Text;
            refSerial.strTitleTip = this.txtTitleTip.Text;
            refSerial.strLiteratureType = this.cmbLiteratureType.Text;
            refSerial.strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            refSerial.strPosition = this.txtPosition.Text;
            refSerial.strPubPlace = this.txtPubPlace.Text;
            refSerial.strPubPeople = this.txtPubPeople.Text;
            refSerial.strPubYear = this.txtPubYear.Text;
            refSerial.strRefTime = this.txtRefTime.Text;
            refSerial.strPath = this.txtPath.Text;
        }

        private bool ValidityCheck(out string errorcode)
        {
            errorcode = "";

            if (string.IsNullOrWhiteSpace(txtMainPeople.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_SERIAL_NEED_MAIN_PEOPLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_SERIAL_NEED_TITLE;
                return false;
            }

            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorcode = "";
            if (ValidityCheck(out errorcode))
            {
                SetRefSerial();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(new ErrorManip.ErrorData(errorcode).ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLiteratureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多类型时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureType.Text == "[.] 更多类型")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureTypePath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_TYPE]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureType = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureType = this.cmbLiteratureType.Text;
            }
        }

        private void cmbLiteratureCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多载体时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureCarrier.Text == "[..] 更多载体")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureCarrierPath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_CARRIER]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureCarrier = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            }
        }

        private void cmbLiteratureType_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }

        private void cmbLiteratureCarrier_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }
    }
}
