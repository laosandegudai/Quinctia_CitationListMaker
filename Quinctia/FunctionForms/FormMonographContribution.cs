﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.FunctionForms
{
    public partial class FormMonographContribution : Form
    {
        public FormMonographContribution()
        {
            InitializeComponent();
        }

        public FormMonographContribution(RefMonographContribution refMonographContribution)
        {
            InitializeComponent();
            SetTxtControls(refMonographContribution);
        }

        private void SetTxtControls(RefMonographContribution refMonographContribution)
        {
            this.txtMainPeople.Text = refMonographContribution.strMainPeople;
            this.txtContributionTitle.Text = refMonographContribution.strContributionTitle;
            this.cmbLiteratureType.Text = refMonographContribution.strLiteratureType;
            this.cmbLiteratureCarrier.Text = refMonographContribution.strLiteratureCarrier;
            this.txtOtherPeople.Text = refMonographContribution.strOtherPeople;
            this.txtMonoMainPeople.Text = refMonographContribution.strMonoMainPeople;
            this.txtMonoTitle.Text = refMonographContribution.strMonoTitle;
            this.txtOtherTitleInfo.Text = refMonographContribution.strOtherTitleInfo;
            this.txtVersion.Text = refMonographContribution.strVersion;
            this.txtPubPlace.Text = refMonographContribution.strPubPlace;
            this.txtPubPeople.Text = refMonographContribution.strPubPeople;
            this.txtPubYear.Text = refMonographContribution.strPubYear;
            this.txtRefPage.Text = refMonographContribution.strRefPage;
            this.txtRefTime.Text = refMonographContribution.strRefTime;
            this.txtPath.Text = refMonographContribution.strPath;
        }

        /// <summary>
        /// 时钟，因为在ComboBox的SelectedIndexChanged事件中无法再次
        /// 改变ComboBox的值，因此需要通过这个时钟在10毫秒后改变
        /// </summary>
        Timer tmrRefreshComboBox = new Timer();

        /// <summary>
        /// 控件cmbLiteratureType的刷新用值
        /// </summary>
        string strLiteratureType = "[M] 专著";
        /// <summary>
        /// 控件cmbLiteratureCarrier的刷新用值
        /// </summary>
        string strLiteratureCarrier = "[  ] 不填写";

        private void FormMonographContribution_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetControlData();
            SetComboBoxTimer();
        }

        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strTitle;
            this.txtIntroduction.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strIntroduction.Replace("|", "\r\n"); ;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strOK;
            this.btnCancel.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strCancel;

            this.lblMainPeople.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strMainPeople.Trim();
            this.lblContributionTitle.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strContributionTitle.Trim();
            this.lblLiteratureType.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strLiteratureType.Trim();
            this.lblOtherPeople.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strOtherPeople.Trim();
            this.lblMonoMainPeople.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strMonoMainPeople.Trim();
            this.lblMonoTitle.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strMonoTitle.Trim();
            this.lblOtherTitleInfo.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strTitleInfo.Trim();
            this.lblVersion.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strVersion.Trim();
            this.lblPubPlace.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strPubPlace.Trim();
            this.lblPubPeople.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strPubPeople.Trim();
            this.lblPubYear.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strPubYear.Trim();
            this.lblRefPage.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strRefPage.Trim();
            this.lblRefTime.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strRefTime.Trim();
            this.lblPath.Text = ConfigHelper.LanguageConfig.langFormMonographContribution.strPath.Trim();
        }

        private void SetControlData()
        {
            cmbLiteratureType.Items.Add("[M] 专著");
            cmbLiteratureType.Items.Add("[J] 期刊");
            cmbLiteratureType.Items.Add("[D] 学位论文");
            cmbLiteratureType.Items.Add("[.] 更多类型");
            cmbLiteratureType.Text = cmbLiteratureType.Items[0].ToString();

            cmbLiteratureCarrier.Items.Add("[  ] 不填写");
            cmbLiteratureCarrier.Items.Add("[CD] 光盘");
            cmbLiteratureCarrier.Items.Add("[OL] 联机网络");
            cmbLiteratureCarrier.Items.Add("[..] 更多载体");
            cmbLiteratureCarrier.Text = cmbLiteratureCarrier.Items[0].ToString();
        }

        /// <summary>
        /// 设置时钟 tmrRefreshComboBox
        /// </summary>
        private void SetComboBoxTimer()
        {
            this.tmrRefreshComboBox.Enabled = true;
            this.tmrRefreshComboBox.Interval = 10;
            this.tmrRefreshComboBox.Tick += (obj, arg) =>
            {
                this.cmbLiteratureType.Text = strLiteratureType;
                this.cmbLiteratureCarrier.Text = strLiteratureCarrier;
                this.tmrRefreshComboBox.Stop();
            };
        }

        public RefMonographContribution refMonographContirbution = null;

        private void SetRefMonographContribution()
        {
            refMonographContirbution = new RefMonographContribution();
            refMonographContirbution.strRefType = ConstantDict.CONSTANT_REF_TYPE_MONOGRAPH_CONTRIBUTION;
            refMonographContirbution.strId = DateTime.Now.ToString("yyyyMMddHHmmssfffff");

            refMonographContirbution.strMainPeople = this.txtMainPeople.Text.Trim();
            refMonographContirbution.strContributionTitle = this.txtContributionTitle.Text.Trim();
            refMonographContirbution.strLiteratureType = this.cmbLiteratureType.Text.Trim();
            refMonographContirbution.strLiteratureCarrier = this.cmbLiteratureCarrier.Text.Trim();
            refMonographContirbution.strOtherPeople = this.txtOtherPeople.Text.Trim();
            refMonographContirbution.strMonoMainPeople = this.txtMonoMainPeople.Text.Trim();
            refMonographContirbution.strMonoTitle = this.txtMonoTitle.Text.Trim();
            refMonographContirbution.strOtherTitleInfo = this.txtOtherTitleInfo.Text.Trim();
            refMonographContirbution.strVersion = this.txtVersion.Text.Trim();
            refMonographContirbution.strPubPlace = this.txtPubPlace.Text.Trim();
            refMonographContirbution.strPubPeople = this.txtPubPeople.Text.Trim();
            refMonographContirbution.strPubYear = this.txtPubYear.Text.Trim();
            refMonographContirbution.strRefPage = this.txtRefPage.Text.Trim();
            refMonographContirbution.strRefTime = this.txtRefTime.Text.Trim();
            refMonographContirbution.strPath = this.txtPath.Text.Trim();
        }

        private bool ValidityCheck(out string errorcode)
        {
            errorcode = "";

            if (string.IsNullOrWhiteSpace(txtMainPeople.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MAIN_PEOPLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtContributionTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_TITLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtMonoMainPeople.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_MAIN_PEOPLE;
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtMonoTitle.Text))
            {
                errorcode = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_TITLE;
                return false;
            }

            return true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errorcode = "";
            if (ValidityCheck(out errorcode))
            {
                SetRefMonographContribution();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(new ErrorManip.ErrorData(errorcode).ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLiteratureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多类型时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureType.Text == "[.] 更多类型")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureTypePath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_TYPE]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureType = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureType = this.cmbLiteratureType.Text;
            }
        }

        private void cmbLiteratureCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //每次选中时更新默认刷新值：
            //选中更多载体时，若返回DialogResult.OK则使用新的刷新值，返回DialogResult.Cancel则使用老的刷新值

            if (cmbLiteratureCarrier.Text == "[..] 更多载体")
            {
                DataSet dsLiteratureCarrier = new DataSet();
                dsLiteratureCarrier.ReadXml(ConfigHelper.QuinctiaConfig.GetLiteratureCarrierPath(), XmlReadMode.Auto);

                FormSelectFromTable formSelectFromTable = new FormSelectFromTable(
                    dsLiteratureCarrier.Tables[ConstantDict.CONSTANT_LANG_DGV_LIT_XML_CARRIER]);

                DialogResult dlgrResult = formSelectFromTable.ShowDialog();
                if (dlgrResult == DialogResult.OK)
                {
                    strLiteratureCarrier = formSelectFromTable.GetCurrentSelection();
                }

                tmrRefreshComboBox.Start();
            }
            else
            {
                strLiteratureCarrier = this.cmbLiteratureCarrier.Text;
            }
        }

        private void cmbLiteratureType_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }

        private void cmbLiteratureCarrier_TextUpdate(object sender, EventArgs e)
        {
            tmrRefreshComboBox.Start();
        }
    }
}
