﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quinctia
{
    class ErrorManip
    {
        /// <summary>
        /// 本程序的异常，包括错误号和异常信息
        /// </summary>
        public class ErrorData
        {
            /// <summary>
            /// 错误号
            /// </summary>
            private string Code = "";
            /// <summary>
            /// 错误信息
            /// </summary>
            private string Info = "";

            /// <summary>
            /// 构造函数
            /// </summary>
            /// <param name="errorcode"></param>
            public ErrorData(string errorcode)
            {
                Code = errorcode;
                Info = GetErrorInfo(errorcode);

                if (string.IsNullOrWhiteSpace(Info))
                {
                    switch (errorcode)
                    {
                        case ConstantDict.CONSTANT_ERROR_NO_LANGUAGE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_NO_LANGUAGE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_LANG_CHANGED:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_LANG_CHANGED_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_NO_ROW_SELECTED:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_NO_ROW_SELECTED_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_TOP_ROW_CANNOT_UP_MORE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_TOP_ROW_CANNOT_UP_MORE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_BOTTOM_ROW_CANNOT_DOWN_MORE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_BOTTOM_ROW_CANNOT_DOWN_MORE_INFO;
                            }
                            break;


                        case ConstantDict.CONSTANT_INFO_GIVE_UP_CHANGED:
                            {
                                Info = ConstantDict.CONSTANT_INFO_GIVE_UP_CHANGED_INFO;
                            }
                            break;

                        case ConstantDict.CONSTANT_ERROR_MONOGRAPH_NEED_MAIN_PEOPLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_MONOGRAPH_NEED_MAIN_PEOPLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_MONOGRAPH_NEED_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_MONOGRAPH_NEED_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MAIN_PEOPLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MAIN_PEOPLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_MAIN_PEOPLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_MAIN_PEOPLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_SERIAL_NEED_MAIN_PEOPLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_SERIAL_NEED_MAIN_PEOPLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_SERIAL_NEED_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_SERIAL_NEED_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_MAIN_PEOPLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_MAIN_PEOPLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_SERIAL_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_SERIAL_CONT_NEED_SERIAL_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_OWNER:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_OWNER_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_COUNTRY:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_COUNTRY_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_PN:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_PATENT_DOC_NEED_PN_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_MAIN_PEOPLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_MAIN_PEOPLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_TITLE:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_TITLE_INFO;
                            }
                            break;
                        case ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_PATH:
                            {
                                Info = ConstantDict.CONSTANT_ERROR_ELECTRONIC_DOC_NEED_PATH_INFO;
                            }
                            break;
                    }
                }
            }

            /// <summary>
            /// 从配置获取异常信息
            /// </summary>
            /// <param name="errorcode"></param>
            /// <returns></returns>
            private string GetErrorInfo(string errorcode)
            {
                return ConfigHelper.LanguageConfig.GetErrorInfo(errorcode);
            }

            /// <summary>
            /// 覆盖原来的ToString函数
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return string.Format("[{0}]: {1}", Code, Info);
            }
        }
    }
}
