﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quinctia
{
    class ConstantDict
    {
        #region 配置节点名（综合）

        /// <summary>
        /// 配置文件路径
        /// </summary>
        public const string CONSTANT_CONFIG_FILENAME = "QuinctiaConfig.xml";

        /// <summary>
        /// 配置文件根节点
        /// </summary>
        public const string CONSTANT_CONFIG_NODE_QUINCTIA_CONFIG = "QuinctiaConfig";

        /// <summary>
        /// 配置文件语言配置节点
        /// </summary>
        public const string CONSTANT_CONFIG_NODE_LANG_CONFIG = "LangConfig";

        /// <summary>
        /// 配置文件语言配置节点：指示当前正在使用的语言
        /// </summary>
        public const string CONSTANT_CONFIG_ATTRIBUTE_CURR_LANG = "CurrLang";
        /// <summary>
        /// 配置文件语言配置节点：是否需要在程序启动时提示语言选择
        /// </summary>
        public const string CONSTANT_CONFIG_ATTRIBUTE_NEED_PROMPT = "NeedPrompt";

        /// <summary>
        /// 配置文件语言配置节点：显示名称
        /// </summary>
        public const string CONSTANT_CONFIG_NODE_LANG = "Lang";

        /// <summary>
        /// 配置文件语言配置节点：显示名称
        /// </summary>
        public const string CONSTANT_CONFIG_ATTRIBUTE_NAME = "Name";

        /// <summary>
        /// 配置文件语言配置节点：对应配置文件地址
        /// </summary>
        public const string CONSTANT_CONFIG_ATTRIBUTE_FILE = "File";


        /// <summary>
        /// 配置文件语言配置节点
        /// </summary>
        public const string CONSTANT_CONFIG_NODE_LITERATURE_CODE_CONFIG = "LiteratureCodeConfig";

        /// <summary>
        /// 配置文件语言配置节点：显示名称
        /// </summary>
        public const string CONSTANT_CONFIG_ATTRIBUTE_CARRIER = "Carrier";

        /// <summary>
        /// 配置文件语言配置节点：对应配置文件地址
        /// </summary>
        public const string CONSTANT_CONFIG_ATTRIBUTE_TYPE = "Type";
        #endregion

        #region 配置节点名（语言）

        public const string CONSTANT_LANG_TEXT = "Text";
        public const string CONSTANT_LANG_NAME = "Name";
        public const string CONSTANT_LANG_COLUMN = "Column";

        /// <summary>
        /// 配置文件根节点
        /// </summary>
        public const string CONSTANT_LANG_LANGUAGE = "Language";


        #region 总界面

        public const string CONSTANT_LANG_FORM_MAIN = "FormMain";

        public const string CONSTANT_LANG_FORM_MAIN_TSMI = "ToolStripMenu";

        public const string CONSTANT_LANG_FORM_MAIN_TSMIF = "ToolStripMenuItemFile";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIF_NEW = "New";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIF_OPEN = "Open";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIF_SAVE = "Save";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIF_GENERATE = "Generate";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIF_CONFIG = "Config";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIF_EXIT = "Exit";

        public const string CONSTANT_LANG_FORM_MAIN_TSMIH = "ToolStripMenuItemHelp";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIH_HELP = "Help";
        public const string CONSTANT_LANG_FORM_MAIN_TSMIH_ABOUT = "About";

        public const string CONSTANT_LANG_FORM_MAIN_CD = "GroupBoxCurrData";

        public const string CONSTANT_LANG_FORM_MAIN_AD = "GroupBoxAddData";
        public const string CONSTANT_LANG_FORM_MAIN_AD_M = "ButtonAddMonograph";
        public const string CONSTANT_LANG_FORM_MAIN_AD_MC = "ButtonAddMonographContribution";
        public const string CONSTANT_LANG_FORM_MAIN_AD_S = "ButtonAddSerial";
        public const string CONSTANT_LANG_FORM_MAIN_AD_SC = "ButtonAddSerialContribution";
        public const string CONSTANT_LANG_FORM_MAIN_AD_PD = "ButtonAddPatentDocumentation";
        public const string CONSTANT_LANG_FORM_MAIN_AD_ED = "ButtonAddElectronicDocument";

        public const string CONSTANT_LANG_FORM_MAIN_OO = "GroupBoxOtherOper";
        public const string CONSTANT_LANG_FORM_MAIN_OO_MODIFY = "ButtonModifySelected";
        public const string CONSTANT_LANG_FORM_MAIN_OO_DELETE = "ButtonDeleteSelected";
        public const string CONSTANT_LANG_FORM_MAIN_OO_CLEAR = "ButtonClear";
        public const string CONSTANT_LANG_FORM_MAIN_OO_GENERATE = "ButtonGenerate";

        public const string CONSTANT_LANG_FORM_MAIN_SS = "StatusStrip";

        #endregion
        #region 文献的增改查界面

        //著作
        public const string CONSTANT_LANG_FORM_MONOGRAPH = "FormMonograph";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_TXT_INTRODUCTION = "TextBoxIntroduction";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_BTN_CANCEL = "ButtonCancel";

        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_MAIN_PEOPLE = "LabelMainPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_TITLE = "LabelTitle";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_TITLE_TIP = "LabelTitleTip";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_LIT_TYPE = "LabelLiteratureType";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_OTHER_PEOPLE = "LabelOtherPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_VERSION = "LabelVersion";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_PUB_PLACE = "LabelPubPlace";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_PUB_PEOPLE = "LabelPubPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_PUB_YEAR = "LabelPubYear";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_REF_PAGE = "LabelRefPage";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_REF_TIME = "LabelRefTime";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_LBL_PATH = "LabelPath";

        //著作析出
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION = "FormMonographContribution";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_TXT_INTRODUCTION = "TextBoxIntroduction";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_BTN_CANCEL = "ButtonCancel";

        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_MAIN_PEOPLE = "LabelMainPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_CONT_TITLE = "LabelContributionTitle";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_LIT_TYPE = "LabelLiteratureType";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_OTHER_PEOPLE = "LabelOtherPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_MONO_MAIN_PEOPLE = "LabelMonoMainPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_MONO_TITLE = "LabelMonoTitle";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_TITLE_INFO = "LabelTitleInfo";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_VERSION = "LabelVersion";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PUB_PLACE = "LabelPubPlace";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PUB_PEOPLE = "LabelPubPeople";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PUB_YEAR = "LabelPubYear";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_REF_PAGE = "LabelRefPage";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_REF_TIME = "LabelRefTime";
        public const string CONSTANT_LANG_FORM_MONOGRAPH_CONTRIBUTION_LBL_PATH = "LabelPath";

        //连续出版物
        public const string CONSTANT_LANG_FORM_SERIAL = "FormSerial";
        public const string CONSTANT_LANG_FORM_SERIAL_TXT_INTRODUCTION = "TextBoxIntroduction";
        public const string CONSTANT_LANG_FORM_SERIAL_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_SERIAL_BTN_CANCEL = "ButtonCancel";

        public const string CONSTANT_LANG_FORM_SERIAL_LBL_MAIN_PEOPLE = "LabelMainPeople";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_TITLE = "LabelTitle";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_TITLE_TIP = "LabelTitleTip";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_LIT_TYPE = "LabelLiteratureType";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_POSITION = "LabelPosition";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_PUB_PLACE = "LabelPubPlace";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_PUB_PEOPLE = "LabelPubPeople";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_PUB_YEAR = "LabelPubYear";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_REF_TIME = "LabelRefTime";
        public const string CONSTANT_LANG_FORM_SERIAL_LBL_PATH = "LabelPath";

        //连续出版物析出
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION = "FormSerialContribution";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_TXT_INTRODUCTION = "TextBoxIntroduction";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_BTN_CANCEL = "ButtonCancel";

        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_MAIN_PEOPLE = "LabelMainPeople";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_TITLE = "LabelTitle";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_LIT_TYPE = "LabelLiteratureType";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_SERIAL_NAME = "LabelSerialName";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_SERIAL_INFO = "LabelSerialInfo";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_POSITION = "LabelPosition";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_REF_TIME = "LabelRefTime";
        public const string CONSTANT_LANG_FORM_SERIAL_CONTRIBUTION_LBL_PATH = "LabelPath";

        //专利
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT = "FormPatentDocument";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_TXT_INTRODUCTION = "TextBoxIntroduction";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENTL_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_BTN_CANCEL = "ButtonCancel";

        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_OWNER = "LabelOwner";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_TITLE = "LabelTitle";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_COUNTRY = "LabelCountry";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_PN = "LabelPN";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_LIT_TYPE = "LabelLiteratureType";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_OPEN_TIME = "LabelOpenTime";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_REF_TIME = "LabelRefTime";
        public const string CONSTANT_LANG_FORM_PATENT_DOCUMENT_LBL_PATH = "LabelPath";

        //电子文献
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT = "FormElectronicDocument";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_TXT_INTRODUCTION = "TextBoxIntroduction";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENTL_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_BTN_CANCEL = "ButtonCancel";

        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_MAIN_PEOPLE = "LabelMainPeople";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_TITLE = "LabelTitle";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_TITLE_TIP = "LabelTitleTip";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_LIT_TYPE = "LabelLiteratureType";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PUB_PLACE = "LabelPubPlace";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PUB_PEOPLE = "LabelPubPeople";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PUB_YEAR = "LabelPubYear";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_LAST_CHANGE = "LabelLastChange";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_REF_TIME = "LabelRefTime";
        public const string CONSTANT_LANG_FORM_ELECTRONIC_DOCUMENT_LBL_PATH = "LabelPath";

        //选则数据表中某数据项界面
        public const string CONSTANT_LANG_FORM_SELECT_FROM_TABLE = "FormSelectFromTable";
        public const string CONSTANT_LANG_FORM_SELECT_FROM_TABLE_GRB_TABLE = "GroupBoxTable";
        public const string CONSTANT_LANG_FORM_SELECT_FROM_TABLE_BTN_OK = "ButtonOK";
        public const string CONSTANT_LANG_FORM_SELECT_FROM_TABLE_BTN_CANCEL = "ButtonCancel";

        #endregion
        #region 关于界面

        public const string CONSTANT_LANG_FORM_ABOUT = "FormAbout";
        public const string CONSTANT_LANG_FORM_ABOUT_TOPIC = "TextBoxTopic";
        public const string CONSTANT_LANG_FORM_ABOUT_ABOUT = "TextBoxAbout";
        public const string CONSTANT_LANG_FORM_ABOUT_OK = "ButtonOK";

        #endregion
        #region 选择语言界面

        public const string CONSTANT_LANG_FORM_SELECT_LANGUAGE = "FormSelectLanguage";
        public const string CONSTANT_LANG_FORM_SELECT_LANGUAGE_TIP = "LabelTip";
        public const string CONSTANT_LANG_FORM_SELECT_LANGUAGE_NO_PROMPT = "CheckBoxNoPrompt";
        public const string CONSTANT_LANG_FORM_SELECT_LANGUAGE_OK = "ButtonOK";

        #endregion
        #region 表格标题

        public const string CONSTANT_LANG_DGV_LIT_CARRIER_TYPE = "DgvCarrierTypeColumnName";

        public const string CONSTANT_LANG_DGV_LIT_COLUMN_ID = "Id";
        public const string CONSTANT_LANG_DGV_LIT_COLUMN_NAME = "Name";
        public const string CONSTANT_LANG_DGV_LIT_COLUMN_SHORT_NAME = "ShortName";
        public const string CONSTANT_LANG_DGV_LIT_COLUMN_DOUBLE_WORD = "DoubleWord";
        public const string CONSTANT_LANG_DGV_LIT_COLUMN_SINGLE_WORD = "SingleWord";
        public const string CONSTANT_LANG_DGV_LIT_COLUMN_COMMENT = "Comment";

        //LiteratureCarrier.xml
        public const string CONSTANT_LANG_DGV_LIT_XML_CARRIERS = "LiteratureCarriers";
        public const string CONSTANT_LANG_DGV_LIT_XML_CARRIER = "LiteratureCarrier";

        //LiteratureType.xml
        public const string CONSTANT_LANG_DGV_LIT_XML_TYPES = "LiteratureTypes";
        public const string CONSTANT_LANG_DGV_LIT_XML_TYPE = "LiteratureType";

        #endregion
        #region 错误信息

        /// <summary>
        /// 配置文件异常信息配置节点
        /// </summary>
        public const string CONSTANT_LANG_NODE_ERRORS = "Errors";
        /// <summary>
        /// 配置文件异常信息配置节点
        /// </summary>
        public const string CONSTANT_LANG_NODE_ERROR = "Error";

        /// <summary>
        /// 配置文件异常信息配置节点：异常编号
        /// </summary>
        public const string CONSTANT_LANG_ATTRIBUTE_CODE = "Code";

        /// <summary>
        /// 配置文件异常信息配置节点：异常信息
        /// </summary>
        public const string CONSTANT_LANG_ATTRIBUTE_INFO = "Info";

        #endregion
        #endregion

        #region 错误信息常量

        //每组错误信息，分为两个常量：第一个常量为错误信息编号，
        //第二个信息为默认信息内容，配置文件未配置则显示此内容，
        //否则用配置文件中的对应错误信息编号内容覆盖

        /// <summary>
        /// 100001：没有找到对应的语言包
        /// </summary>
        public const string CONSTANT_ERROR_NO_LANGUAGE = "100001";
        public const string CONSTANT_ERROR_NO_LANGUAGE_INFO = "没有找到对应的语言包";

        /// <summary>
        /// 100002：语言包设定完毕，重新启动后将更换新的语言包
        /// </summary>
        public const string CONSTANT_ERROR_LANG_CHANGED = "100002";
        public const string CONSTANT_ERROR_LANG_CHANGED_INFO = "新的语言设置将在程序重新启动后启用";

        /// <summary>
        /// 100003：未选择任何一个数据列
        /// </summary>
        public const string CONSTANT_ERROR_NO_ROW_SELECTED = "100003";
        public const string CONSTANT_ERROR_NO_ROW_SELECTED_INFO = "未选择任何一个数据行";

        /// <summary>
        /// 100004：未选择任何一个数据列
        /// </summary>
        public const string CONSTANT_ERROR_TOP_ROW_CANNOT_UP_MORE = "100004";
        public const string CONSTANT_ERROR_TOP_ROW_CANNOT_UP_MORE_INFO = "顶行数据无法上移";

        /// <summary>
        /// 100005：未选择任何一个数据列
        /// </summary>
        public const string CONSTANT_ERROR_BOTTOM_ROW_CANNOT_DOWN_MORE = "100005";
        public const string CONSTANT_ERROR_BOTTOM_ROW_CANNOT_DOWN_MORE_INFO = "尾行数据无法下移";

        /// <summary>
        /// 200001：确定要放弃之前改动的数据吗，未保存的信息都将丢失
        /// </summary>
        public const string CONSTANT_INFO_GIVE_UP_CHANGED = "200001";
        public const string CONSTANT_INFO_GIVE_UP_CHANGED_INFO = "确定要放弃之前改动的数据吗，未保存的信息都将丢失";


        /// <summary>
        /// 300001：专著需要有至少一个主要责任者
        /// </summary>
        public const string CONSTANT_ERROR_MONOGRAPH_NEED_MAIN_PEOPLE = "300001";
        public const string CONSTANT_ERROR_MONOGRAPH_NEED_MAIN_PEOPLE_INFO = "专著需要有至少一个主要责任者";

        /// <summary>
        /// 300002：专著需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_MONOGRAPH_NEED_TITLE = "300002";
        public const string CONSTANT_ERROR_MONOGRAPH_NEED_TITLE_INFO = "专著需要题名信息";

        /// <summary>
        /// 300003：专著的析出文献需要有至少一个主要责任者
        /// </summary>
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MAIN_PEOPLE = "300003";
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MAIN_PEOPLE_INFO = "专著的析出文献需要有至少一个主要责任者";

        /// <summary>
        /// 300004：专著的析出文献需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_TITLE = "300004";
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_TITLE_INFO = "专著的析出文献需要题名信息";

        /// <summary>
        /// 300005：专著的析出文献所在专著需要有至少一个主要责任者
        /// </summary>
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_MAIN_PEOPLE = "300005";
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_MAIN_PEOPLE_INFO = "专著的析出文献所在专著需要有至少一个主要责任者";

        /// <summary>
        /// 300006：专著的析出文献所在专著需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_TITLE = "300006";
        public const string CONSTANT_ERROR_MONOGRAPH_CONT_NEED_MONO_TITLE_INFO = "专著的析出文献所在专著需要题名信息";

        /// <summary>
        /// 300007：连续出版物需要至少一个主要责任者
        /// </summary>
        public const string CONSTANT_ERROR_SERIAL_NEED_MAIN_PEOPLE = "300007";
        public const string CONSTANT_ERROR_SERIAL_NEED_MAIN_PEOPLE_INFO = "连续出版物需要至少一个主要责任者";

        /// <summary>
        /// 300008：连续出版物需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_SERIAL_NEED_TITLE = "300008";
        public const string CONSTANT_ERROR_SERIAL_NEED_TITLE_INFO = "连续出版物需要题名信息";

        /// <summary>
        /// 300009：连续出版物的析出文献需要至少一个主要责任者
        /// </summary>
        public const string CONSTANT_ERROR_SERIAL_CONT_NEED_MAIN_PEOPLE = "300009";
        public const string CONSTANT_ERROR_SERIAL_CONT_NEED_MAIN_PEOPLE_INFO = "连续出版物的析出文献需要至少一个主要责任者";

        /// <summary>
        /// 300010：连续出版物的析出文献需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_SERIAL_CONT_NEED_TITLE = "300010";
        public const string CONSTANT_ERROR_SERIAL_CONT_NEED_TITLE_INFO = "连续出版物的析出文献需要题名信息";

        /// <summary>
        /// 300011：连续出版物的析出文献所在连续出版物需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_SERIAL_CONT_NEED_SERIAL_TITLE = "300011";
        public const string CONSTANT_ERROR_SERIAL_CONT_NEED_SERIAL_TITLE_INFO = "连续出版物的析出文献所在连续出版物需要题名信息";

        /// <summary>
        /// 300012：专利需要有至少一个申请者或所有者
        /// </summary>
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_OWNER = "300012";
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_OWNER_INFO = "专利需要有至少一个申请者或所有者";

        /// <summary>
        /// 300013：专利需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_TITLE = "300013";
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_TITLE_INFO = "专利需要题名信息";

        /// <summary>
        /// 300014：专利需要所在国别
        /// </summary>
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_COUNTRY = "300014";
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_COUNTRY_INFO = "专利需要所在国别";

        /// <summary>
        /// 300015：专利需要专利号
        /// </summary>
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_PN = "300015";
        public const string CONSTANT_ERROR_PATENT_DOC_NEED_PN_INFO = "专利需要专利号";

        /// <summary>
        /// 300016：电子文献需要有至少一个主要责任者
        /// </summary>
        public const string CONSTANT_ERROR_ELECTRONIC_DOC_NEED_MAIN_PEOPLE = "300016";
        public const string CONSTANT_ERROR_ELECTRONIC_DOC_NEED_MAIN_PEOPLE_INFO = "电子文献需要有至少一个主要责任者";

        /// <summary>
        /// 300017：电子文献需要题名信息
        /// </summary>
        public const string CONSTANT_ERROR_ELECTRONIC_DOC_NEED_TITLE = "300017";
        public const string CONSTANT_ERROR_ELECTRONIC_DOC_NEED_TITLE_INFO = "电子文献需要题名信息";

        /// <summary>
        /// 300018：电子文献需要获取和访问路径
        /// </summary>
        public const string CONSTANT_ERROR_ELECTRONIC_DOC_NEED_PATH = "300018";
        public const string CONSTANT_ERROR_ELECTRONIC_DOC_NEED_PATH_INFO = "电子文献需要获取和访问路径";

        #endregion

        #region 其他 XML读取和保存标志

        public const string CONSTANT_REF_REFERENCES = "References";
        public const string CONSTANT_REF_REFERENCE = "Reference";

        public const string CONSTANT_REF_TYPE_MONOGRAPH = "Monograph";
        public const string CONSTANT_REF_TYPE_MONOGRAPH_CONTRIBUTION = "MonographContribution";
        public const string CONSTANT_REF_TYPE_SERIAL = "Serial";
        public const string CONSTANT_REF_TYPE_SERIAL_CONTRIBUTION = "SerialContribution";
        public const string CONSTANT_REF_TYPE_PATENT_DOCUMENT = "PatentDocument";
        public const string CONSTANT_REF_TYPE_ELECTRONIC_DOCUMENT = "ElectronicDocument";

        public const string CONSTANT_REF_ID = "RefId";
        public const string CONSTANT_REF_TYPE = "RefType";

        #region 专著

        public const string CONSTANT_REF_MONOGRAPH_MAIN_PEOPLE = "MainPeople";
        public const string CONSTANT_REF_MONOGRAPH_TITLE = "Title";
        public const string CONSTANT_REF_MONOGRAPH_TITLE_TIP = "TitleTip";
        public const string CONSTANT_REF_MONOGRAPH_LITERATURE_TYPE = "LiteratureType";
        public const string CONSTANT_REF_MONOGRAPH_LITERATURE_CARRIER = "LiteratureCarrier";
        public const string CONSTANT_REF_MONOGRAPH_OTHER_PEOPLE = "OtherPeople";
        public const string CONSTANT_REF_MONOGRAPH_VERSION = "Version";
        public const string CONSTANT_REF_MONOGRAPH_PUB_PLACE = "PubPlace";
        public const string CONSTANT_REF_MONOGRAPH_PUB_PEOPLE = "PubPeople";
        public const string CONSTANT_REF_MONOGRAPH_PUB_YEAR = "PubYear";
        public const string CONSTANT_REF_MONOGRAPH_REF_PAGE = "RefPage";
        public const string CONSTANT_REF_MONOGRAPH_REF_TIME = "RefTime";
        public const string CONSTANT_REF_MONOGRAPH_PATH = "Path";

        #endregion
        #region 专著析出文献

        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MAIN_PEOPLE = "MainPeople";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_CON_TITLE = "ContributionTitle";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_LIT_TYPE = "LiteratureType";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_LIT_CARRIER = "LiteratureCarrier";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_OTHER_PEOPLE = "OtherPeople";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MONO_MAIN_PEOPLE = "MonoMainPeople";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MONO_TITLE = "MonoTitle";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_OTHER_TITLE_INFO = "OtherTitleInfo";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_VERSION = "Version";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_PLACE = "PubPlace";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_PEOPLE = "PubPeople";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_YEAR = "PubYear";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_REF_PAGE = "RefPage";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_REF_TIME = "RefTime";
        public const string CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PATH = "Path";

        #endregion
        #region 连续出版物

        public const string CONSTANT_REF_SERIAL_MAIN_PEOPLE = "MainPeople";
        public const string CONSTANT_REF_SERIAL_TITLE = "Title";
        public const string CONSTANT_REF_SERIAL_TITLE_TIP = "TitleTip";
        public const string CONSTANT_REF_SERIAL_LIT_TYPE = "LiteratureType";
        public const string CONSTANT_REF_SERIAL_LIT_CARRIER = "LiteratureCarrier";
        public const string CONSTANT_REF_SERIAL_POSITION = "Position";
        public const string CONSTANT_REF_SERIAL_PUB_PLACE = "PubPlace";
        public const string CONSTANT_REF_SERIAL_PUB_PEOPLE = "PubPeople";
        public const string CONSTANT_REF_SERIAL_PUB_YEAR = "PubYear";
        public const string CONSTANT_REF_SERIAL_REF_TIME = "RefTime";
        public const string CONSTANT_REF_SERIAL_PATH = "Path";

        #endregion
        #region 连续出版物析出文献

        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_MAIN_PEOPLE = "MainPeople";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_TITLE = "Title";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_LIT_TYPE = "LiteratureType";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_LIT_CARRIER = "LiteratureCarrier";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_SERIAL_NAME = "SerialName";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_SERIAL_INFO = "SerialInfo";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_POSITION = "Position";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_PLACE = "PubPlace";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_PEOPLE = "PubPeople";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_YEAR = "PubYear";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_REF_TIME = "RefTime";
        public const string CONSTANT_REF_SERIAL_CONTRIBUTION_PATH = "Path";

        #endregion
        #region 专利文献

        public const string CONSTANT_REF_PATENT_DOC_OWNER = "Owner";
        public const string CONSTANT_REF_PATENT_DOC_TITLE = "Title";
        public const string CONSTANT_REF_PATENT_DOC_COUNTRY = "Country";
        public const string CONSTANT_REF_PATENT_DOC_PN = "PN";
        public const string CONSTANT_REF_PATENT_DOC_LIT_TYPE = "LiteratureType";
        public const string CONSTANT_REF_PATENT_DOC_LIT_CARRIER = "LiteratureCarrier";
        public const string CONSTANT_REF_PATENT_DOC_OPEN_TIME = "OpenTime";
        public const string CONSTANT_REF_PATENT_DOC_REF_TIME = "RefTime";
        public const string CONSTANT_REF_PATENT_DOC_PATH = "Path";

        #endregion
        #region 电子文献

        public const string CONSTANT_REF_ELECTRONIC_DOC_MAIN_PEOPLE = "MainPeople";
        public const string CONSTANT_REF_ELECTRONIC_DOC_TITLE = "Title";
        public const string CONSTANT_REF_ELECTRONIC_DOC_TITLE_TIP = "TitleTip";
        public const string CONSTANT_REF_ELECTRONIC_DOC_LIT_TYPE = "LiteratureType";
        public const string CONSTANT_REF_ELECTRONIC_DOC_LIT_CARRIER = "LiteratureCarrier";
        public const string CONSTANT_REF_ELECTRONIC_DOC_PUB_PLACE = "PubPlace";
        public const string CONSTANT_REF_ELECTRONIC_DOC_PUB_PEOPLE = "PubPeople";
        public const string CONSTANT_REF_ELECTRONIC_DOC_PUB_YEAR = "PubYear";
        public const string CONSTANT_REF_ELECTRONIC_DOC_LAST_CHANGE = "LastChange";
        public const string CONSTANT_REF_ELECTRONIC_DOC_REF_TIME = "RefTime";
        public const string CONSTANT_REF_ELECTRONIC_DOC_PATH = "Path";

        #endregion

        #endregion
    }
}
