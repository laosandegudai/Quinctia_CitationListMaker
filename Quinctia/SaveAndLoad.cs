﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;

namespace Quinctia
{
    class SaveAndLoad
    {
        /// <summary>
        /// 从一个XML文件中读取文档内容
        /// </summary>
        /// <param name="llReferences"></param>
        /// <param name="xmlpath"></param>
        public static void ReadRefListFromXML(LinkedList<Reference> llReferences, string xmlpath)
        {
            llReferences.Clear();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlpath);

            XmlNode root = xmlDoc.SelectSingleNode(ConstantDict.CONSTANT_REF_REFERENCES);
            foreach (XmlNode xnReference in root.ChildNodes)
            {
                //只有读取到XmlElement才进一步读取
                if (xnReference.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeReference = (XmlElement)xnReference;

                    string temp = xeReference.GetAttribute(ConstantDict.CONSTANT_REF_TYPE);
                    switch (temp)
                    {
                        case ConstantDict.CONSTANT_REF_TYPE_MONOGRAPH:
                            {
                                llReferences.AddLast(GetRefMonograph(xeReference));
                            }
                            break;
                        case ConstantDict.CONSTANT_REF_TYPE_MONOGRAPH_CONTRIBUTION:
                            {
                                llReferences.AddLast(GetRefMonographContribution(xeReference));
                            }
                            break;
                        case ConstantDict.CONSTANT_REF_TYPE_SERIAL:
                            {
                                llReferences.AddLast(GetRefSerial(xeReference));
                            }
                            break;
                        case ConstantDict.CONSTANT_REF_TYPE_SERIAL_CONTRIBUTION:
                            {
                                llReferences.AddLast(GetRefSerialContribution(xeReference));
                            }
                            break;
                        case ConstantDict.CONSTANT_REF_TYPE_PATENT_DOCUMENT:
                            {
                                llReferences.AddLast(GetRefPatentDocument(xeReference));
                            }
                            break;
                        case ConstantDict.CONSTANT_REF_TYPE_ELECTRONIC_DOCUMENT:
                            {
                                llReferences.AddLast(GetRefElectronicDocument(xeReference));
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 读取专著 Monograph
        /// </summary>
        /// <param name="xnReference"></param>
        /// <returns></returns>
        private static RefMonograph GetRefMonograph(XmlNode xnReference)
        {
            RefMonograph refMonograph = new RefMonograph();

            refMonograph.strId = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_ID);
            refMonograph.strRefType = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_TYPE);

            foreach (XmlNode xnElement in xnReference.ChildNodes)
            {
                if (xnElement.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeElement = (XmlElement)xnElement;

                    switch (xeElement.Name)
                    {
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_MAIN_PEOPLE: refMonograph.strMainPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_TITLE: refMonograph.strTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_TITLE_TIP: refMonograph.strTitleTip = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_LITERATURE_TYPE: refMonograph.strLiteratureType = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_LITERATURE_CARRIER: refMonograph.strLiteratureCarrier = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_OTHER_PEOPLE: refMonograph.strOtherPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_VERSION: refMonograph.strVersion = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_PUB_PLACE: refMonograph.strPubPlace = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_PUB_PEOPLE: refMonograph.strPubPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_PUB_YEAR: refMonograph.strPubYear = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_REF_PAGE: refMonograph.strRefPage = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_REF_TIME: refMonograph.strRefTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_PATH: refMonograph.strPath = xeElement.InnerText; break;
                    }
                }
            }

            return refMonograph;
        }

        /// <summary>
        /// 读取专著析出文献 MonographContribution
        /// </summary>
        /// <param name="xnReference"></param>
        /// <returns></returns>
        private static RefMonographContribution GetRefMonographContribution(XmlNode xnReference)
        {
            RefMonographContribution refMonographContribution = new RefMonographContribution();

            refMonographContribution.strId = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_ID);
            refMonographContribution.strRefType = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_TYPE);

            foreach (XmlNode xnElement in xnReference.ChildNodes)
            {
                if (xnElement.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeElement = (XmlElement)xnElement;

                    switch (xeElement.Name)
                    {
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MAIN_PEOPLE: refMonographContribution.strMainPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_CON_TITLE: refMonographContribution.strContributionTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_LIT_TYPE: refMonographContribution.strLiteratureType = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_LIT_CARRIER: refMonographContribution.strLiteratureCarrier = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_OTHER_PEOPLE: refMonographContribution.strOtherPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MONO_MAIN_PEOPLE: refMonographContribution.strMonoMainPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MONO_TITLE: refMonographContribution.strMonoTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_OTHER_TITLE_INFO: refMonographContribution.strOtherTitleInfo = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_VERSION: refMonographContribution.strVersion = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_PLACE: refMonographContribution.strPubPlace = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_PEOPLE: refMonographContribution.strPubPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_YEAR: refMonographContribution.strPubYear = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_REF_PAGE: refMonographContribution.strRefPage = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_REF_TIME: refMonographContribution.strRefTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PATH: refMonographContribution.strPath = xeElement.InnerText; break;
                    }
                }
            }

            return refMonographContribution;
        }

        /// <summary>
        /// 读取连续出版物 Serial
        /// </summary>
        /// <param name="xnReference"></param>
        /// <returns></returns>
        private static RefSerial GetRefSerial(XmlNode xnReference)
        {
            RefSerial refSerial = new RefSerial();

            refSerial.strId = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_ID);
            refSerial.strRefType = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_TYPE);

            foreach (XmlNode xnElement in xnReference.ChildNodes)
            {
                if (xnElement.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeElement = (XmlElement)xnElement;

                    switch (xeElement.Name)
                    {
                        case ConstantDict.CONSTANT_REF_SERIAL_MAIN_PEOPLE: refSerial.strMainPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_TITLE: refSerial.strTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_TITLE_TIP: refSerial.strTitleTip = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_LIT_TYPE: refSerial.strLiteratureType = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_LIT_CARRIER: refSerial.strLiteratureCarrier = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_POSITION: refSerial.strPosition = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_PUB_PLACE: refSerial.strPubPlace = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_PUB_PEOPLE: refSerial.strPubPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_PUB_YEAR: refSerial.strPubYear = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_REF_TIME: refSerial.strRefTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_PATH: refSerial.strPath = xeElement.InnerText; break;
                    }
                }
            }

            return refSerial;
        }

        /// <summary>
        /// 读取连续出版物 SerialContribution
        /// </summary>
        /// <param name="xnReference"></param>
        /// <returns></returns>
        private static RefSerialContribution GetRefSerialContribution(XmlNode xnReference)
        {
            RefSerialContribution refSerialContribution = new RefSerialContribution();

            refSerialContribution.strId = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_ID);
            refSerialContribution.strRefType = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_TYPE);

            foreach (XmlNode xnElement in xnReference.ChildNodes)
            {
                if (xnElement.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeElement = (XmlElement)xnElement;

                    switch (xeElement.Name)
                    {
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_MAIN_PEOPLE: refSerialContribution.strMainPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_TITLE: refSerialContribution.strTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_LIT_TYPE: refSerialContribution.strLiteratureType = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_LIT_CARRIER: refSerialContribution.strLiteratureCarrier = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_SERIAL_NAME: refSerialContribution.strSerialName = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_SERIAL_INFO: refSerialContribution.strSerialInfo = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_POSITION: refSerialContribution.strPosition = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_PLACE: refSerialContribution.strPubPlace = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_PEOPLE: refSerialContribution.strPubPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_YEAR: refSerialContribution.strPubYear = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_REF_TIME: refSerialContribution.strRefTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PATH: refSerialContribution.strPath = xeElement.InnerText; break;
                    }
                }
            }

            return refSerialContribution;
        }

        /// <summary>
        /// 读取专利文献 PatentDocument
        /// </summary>
        /// <param name="xnReference"></param>
        /// <returns></returns>
        private static RefPatentDocument GetRefPatentDocument(XmlNode xnReference)
        {
            RefPatentDocument refPatentDocument = new RefPatentDocument();

            refPatentDocument.strId = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_ID);
            refPatentDocument.strRefType = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_TYPE);

            foreach (XmlNode xnElement in xnReference.ChildNodes)
            {
                if (xnElement.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeElement = (XmlElement)xnElement;

                    switch (xeElement.Name)
                    {
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_OWNER: refPatentDocument.strOwner = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_TITLE: refPatentDocument.strTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_COUNTRY: refPatentDocument.strCountry = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_PN: refPatentDocument.strPN = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_LIT_TYPE: refPatentDocument.strLiteratureType = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_LIT_CARRIER: refPatentDocument.strLiteratureCarrier = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_OPEN_TIME: refPatentDocument.strOpenTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_REF_TIME: refPatentDocument.strRefTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_PATENT_DOC_PATH: refPatentDocument.strPath = xeElement.InnerText; break;
                    }
                }
            }

            return refPatentDocument;
        }

        /// <summary>
        /// 读取电子文献 ElectronicDocument
        /// </summary>
        /// <param name="xnReference"></param>
        /// <returns></returns>
        private static RefElectronicDocument GetRefElectronicDocument(XmlNode xnReference)
        {
            RefElectronicDocument refElectronicDocument = new RefElectronicDocument();

            refElectronicDocument.strId = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_ID);
            refElectronicDocument.strRefType = (xnReference as XmlElement).GetAttribute(ConstantDict.CONSTANT_REF_TYPE);

            foreach (XmlNode xnElement in xnReference.ChildNodes)
            {
                if (xnElement.GetType().ToString() == "System.Xml.XmlElement")
                {
                    XmlElement xeElement = (XmlElement)xnElement;

                    switch (xeElement.Name)
                    {
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_MAIN_PEOPLE: refElectronicDocument.strMainPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_TITLE: refElectronicDocument.strTitle = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_TITLE_TIP: refElectronicDocument.strTitleTip = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_LIT_TYPE: refElectronicDocument.strLiteratureType = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_LIT_CARRIER: refElectronicDocument.strLiteratureCarrier = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PUB_PLACE: refElectronicDocument.strPubPlace = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PUB_PEOPLE: refElectronicDocument.strPubPeople = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PUB_YEAR: refElectronicDocument.strPubYear = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_LAST_CHANGE: refElectronicDocument.strLastChange = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_REF_TIME: refElectronicDocument.strRefTime = xeElement.InnerText; break;
                        case ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PATH: refElectronicDocument.strPath = xeElement.InnerText; break;
                    }
                }
            }

            return refElectronicDocument;
        }

        /// <summary>
        /// 向一个XML中写入当前引用表
        /// </summary>
        /// <param name="llReferences"></param>
        /// <param name="xmlpath"></param>
        public static void WriteRefListToXML(LinkedList<Reference> llReferences, string xmlpath)
        {
            XmlTextWriter writer = new XmlTextWriter(xmlpath, Encoding.GetEncoding("utf-8"));
            writer.WriteStartDocument();
            writer.WriteStartElement(ConstantDict.CONSTANT_REF_REFERENCES);

            LinkedListNode<Reference> llnTemp = llReferences.First;
            while (llnTemp != null)
            {
                writer.WriteStartElement(ConstantDict.CONSTANT_REF_REFERENCE);

                switch (llnTemp.Value.strRefType)
                {
                    case ConstantDict.CONSTANT_REF_TYPE_MONOGRAPH:
                        {
                            RefMonograph refMonograph = llnTemp.Value as RefMonograph;

                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_ID, refMonograph.strId);
                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_TYPE, refMonograph.strRefType);

                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_MAIN_PEOPLE, refMonograph.strMainPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_TITLE, refMonograph.strTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_TITLE_TIP, refMonograph.strTitleTip);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_LITERATURE_TYPE, refMonograph.strLiteratureType);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_LITERATURE_CARRIER, refMonograph.strLiteratureCarrier);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_OTHER_PEOPLE, refMonograph.strOtherPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_VERSION, refMonograph.strVersion);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_PUB_PLACE, refMonograph.strPubPlace);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_PUB_PEOPLE, refMonograph.strPubPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_PUB_YEAR, refMonograph.strPubYear);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_REF_PAGE, refMonograph.strRefPage);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_REF_TIME, refMonograph.strRefTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_PATH, refMonograph.strPath);
                        }
                        break;
                    case ConstantDict.CONSTANT_REF_TYPE_MONOGRAPH_CONTRIBUTION:
                        {
                            RefMonographContribution refMonographContribution = llnTemp.Value as RefMonographContribution;

                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_ID, refMonographContribution.strId);
                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_TYPE, refMonographContribution.strRefType);

                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MAIN_PEOPLE, refMonographContribution.strMainPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_CON_TITLE, refMonographContribution.strContributionTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_LIT_TYPE, refMonographContribution.strLiteratureType);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_LIT_CARRIER, refMonographContribution.strLiteratureCarrier);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_OTHER_PEOPLE, refMonographContribution.strOtherPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MONO_MAIN_PEOPLE, refMonographContribution.strMonoMainPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_MONO_TITLE, refMonographContribution.strMonoTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_OTHER_TITLE_INFO, refMonographContribution.strOtherTitleInfo);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_VERSION, refMonographContribution.strVersion);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_PLACE, refMonographContribution.strPubPlace);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_PEOPLE, refMonographContribution.strPubPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PUB_YEAR, refMonographContribution.strPubYear);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_REF_PAGE, refMonographContribution.strRefPage);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_REF_TIME, refMonographContribution.strRefTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_MONOGRAPH_CONTRIBUTION_PATH, refMonographContribution.strPath);
                        }
                        break;
                    case ConstantDict.CONSTANT_REF_TYPE_SERIAL:
                        {
                            RefSerial refSerial = llnTemp.Value as RefSerial;

                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_ID, refSerial.strId);
                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_TYPE, refSerial.strRefType);

                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_MAIN_PEOPLE, refSerial.strMainPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_TITLE, refSerial.strTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_TITLE_TIP, refSerial.strTitleTip);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_LIT_TYPE, refSerial.strLiteratureType);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_LIT_CARRIER, refSerial.strLiteratureCarrier);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_POSITION, refSerial.strPosition);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_PUB_PLACE, refSerial.strPubPlace);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_PUB_PEOPLE, refSerial.strPubPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_PUB_YEAR, refSerial.strPubYear);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_REF_TIME, refSerial.strRefTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_PATH, refSerial.strPath);
                        }
                        break;
                    case ConstantDict.CONSTANT_REF_TYPE_SERIAL_CONTRIBUTION:
                        {
                            RefSerialContribution refSerialContribution = llnTemp.Value as RefSerialContribution;

                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_ID, refSerialContribution.strId);
                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_TYPE, refSerialContribution.strRefType);

                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_MAIN_PEOPLE, refSerialContribution.strMainPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_TITLE, refSerialContribution.strTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_LIT_TYPE, refSerialContribution.strLiteratureType);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_LIT_CARRIER, refSerialContribution.strLiteratureCarrier);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_SERIAL_NAME, refSerialContribution.strSerialName);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_SERIAL_INFO, refSerialContribution.strSerialInfo);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_POSITION, refSerialContribution.strPosition);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_PLACE, refSerialContribution.strPubPlace);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_PEOPLE, refSerialContribution.strPubPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PUB_YEAR, refSerialContribution.strPubYear);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_REF_TIME, refSerialContribution.strRefTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_SERIAL_CONTRIBUTION_PATH, refSerialContribution.strPath);
                        }
                        break;
                    case ConstantDict.CONSTANT_REF_TYPE_PATENT_DOCUMENT:
                        {
                            RefPatentDocument refPatentDocument = llnTemp.Value as RefPatentDocument;

                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_ID, refPatentDocument.strId);
                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_TYPE, refPatentDocument.strRefType);

                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_OWNER, refPatentDocument.strOwner); 
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_TITLE, refPatentDocument.strTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_COUNTRY, refPatentDocument.strCountry);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_PN, refPatentDocument.strPN);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_LIT_TYPE, refPatentDocument.strLiteratureType);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_LIT_CARRIER, refPatentDocument.strLiteratureCarrier);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_OPEN_TIME, refPatentDocument.strOpenTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_REF_TIME, refPatentDocument.strRefTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_PATENT_DOC_PATH, refPatentDocument.strPath);
                        }
                        break;
                    case ConstantDict.CONSTANT_REF_TYPE_ELECTRONIC_DOCUMENT:
                        {
                            RefElectronicDocument refElectronicDocument = llnTemp.Value as RefElectronicDocument;

                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_ID, refElectronicDocument.strId);
                            writer.WriteAttributeString(ConstantDict.CONSTANT_REF_TYPE, refElectronicDocument.strRefType);

                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_MAIN_PEOPLE, refElectronicDocument.strMainPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_TITLE, refElectronicDocument.strTitle);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_TITLE_TIP, refElectronicDocument.strTitleTip);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_LIT_TYPE, refElectronicDocument.strLiteratureType);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_LIT_CARRIER, refElectronicDocument.strLiteratureCarrier);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PUB_PLACE, refElectronicDocument.strPubPlace);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PUB_PEOPLE, refElectronicDocument.strPubPeople);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PUB_YEAR, refElectronicDocument.strPubYear);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_LAST_CHANGE, refElectronicDocument.strLastChange);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_REF_TIME, refElectronicDocument.strRefTime);
                            writer.WriteElementString(ConstantDict.CONSTANT_REF_ELECTRONIC_DOC_PATH, refElectronicDocument.strPath);
                        }
                        break;
                }

                writer.WriteEndElement();
                llnTemp = llnTemp.Next;
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }
    }
}
