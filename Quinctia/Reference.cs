﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quinctia
{
    #region 引用基类

    /// <summary>
    /// 著录引用
    /// </summary>
    public class Reference
    {
        /// <summary>
        /// 类型
        /// </summary>
        public string strRefType;
        /// <summary>
        /// 唯一标识
        /// </summary>
        public string strId;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// 规范化人员名单，按逗号分隔，取前三个人，后面的用“等”字代替
        /// </summary>
        /// <param name="strPeopleList">人员名单</param>
        /// <returns></returns>
        protected string NormalizePeopleList(string strPeopleList)
        {
            string strTempPeopleList = strPeopleList.Replace("，", ",");
            string[] strList = strTempPeopleList.Split(',');

            string strResultList = "";
            if (strList.Length >= 1)
            {
                strResultList += strList[0];
            }
            if (strList.Length >= 2)
            {
                strResultList += ",";
                strResultList += strList[1];
            }
            if (strList.Length >= 3)
            {
                strResultList += ",";
                strResultList += strList[2];
            }
            if (strList.Length >= 4)
            {
                strResultList += ",等";
            }

            return strResultList;
        }
    }

    #endregion
    #region 专著

    /// <summary>
    /// 专著
    /// </summary>
    public class RefMonograph : Reference
    {
        public string strMainPeople;
        public string strTitle;
        public string strTitleTip;
        public string strLiteratureType;
        public string strLiteratureCarrier;
        public string strOtherPeople;
        public string strVersion;
        public string strPubPlace;
        public string strPubPeople;
        public string strPubYear;
        public string strRefPage;
        public string strRefTime;
        public string strPath;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sbReferenceString = new StringBuilder();

            //主要责任者
            if (!string.IsNullOrWhiteSpace(strMainPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strMainPeople));
            }
            else
            {
                sbReferenceString.Append("未知出版者");
            }
            sbReferenceString.Append(".");
            sbReferenceString.Append(" ");

            //题名 其他题名信息 文件类型标志
            if (!string.IsNullOrWhiteSpace(strTitle))
            {
                sbReferenceString.Append(strTitle);

                //其他题名信息
                if (!string.IsNullOrWhiteSpace(strTitleTip))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strTitleTip);
                }
            }
            else
            {
                sbReferenceString.Append("未知文献");
            }

            sbReferenceString.Append(" ");

            //文献类型标志（文献类型/文献载体）
            sbReferenceString.Append("[");

            string strType = strLiteratureType.Substring(strLiteratureType.IndexOf('[') + 1, strLiteratureType.IndexOf(']') - 1);
            string strCarrier = strLiteratureCarrier.Substring(strLiteratureCarrier.IndexOf('[') + 1, strLiteratureCarrier.IndexOf(']') - 1);

            sbReferenceString.Append(strType);
            if (!string.IsNullOrWhiteSpace(strCarrier))
            {
                sbReferenceString.Append("/");
                sbReferenceString.Append(strCarrier);
            }

            sbReferenceString.Append("].");
            sbReferenceString.Append(" ");

            //其他责任者
            if (!string.IsNullOrWhiteSpace(strOtherPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strOtherPeople));
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //版本项
            if (!string.IsNullOrWhiteSpace(strVersion))
            {
                sbReferenceString.Append(strVersion);
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //出版地 出版者 出版年 引文页码 引用日期
            if (!string.IsNullOrWhiteSpace(strPubPeople))
            {
                //出版地（可选，所以冒号放在它里面）
                if (!string.IsNullOrWhiteSpace(strPubPlace))
                {
                    sbReferenceString.Append(strPubPlace);
                    sbReferenceString.Append(":");
                }

                sbReferenceString.Append(strPubPeople);
                sbReferenceString.Append(",");

                if (!string.IsNullOrWhiteSpace(strPubYear))
                {
                    sbReferenceString.Append(strPubYear);
                }

                //引用页码（可选，所以冒号放在它里面）
                if (!string.IsNullOrWhiteSpace(strRefPage))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strRefPage);
                    sbReferenceString.Append(".");
                }

                sbReferenceString.Append(" ");
            }

            //引用时间
            if (!string.IsNullOrWhiteSpace(strRefTime))
            {
                sbReferenceString.Append("[");
                sbReferenceString.Append(strRefTime);
                sbReferenceString.Append("]");
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //获取和访问路径
            if (!string.IsNullOrWhiteSpace(strPath))
            {
                sbReferenceString.Append(strPath);
                sbReferenceString.Append(".");
            }

            return sbReferenceString.ToString();
        }
    }

    #endregion
    #region 专著中的析出文献

    /// <summary>
    /// 专著中的析出文献
    /// </summary>
    public class RefMonographContribution : Reference
    {
        public string strMainPeople;
        public string strContributionTitle;
        public string strLiteratureType;
        public string strLiteratureCarrier;
        public string strOtherPeople;
        public string strMonoMainPeople;
        public string strMonoTitle;
        public string strOtherTitleInfo;
        public string strVersion;
        public string strPubPlace;
        public string strPubPeople;
        public string strPubYear;
        public string strRefPage;
        public string strRefTime;
        public string strPath;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sbReferenceString = new StringBuilder();

            //主要责任者
            if (!string.IsNullOrWhiteSpace(strMainPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strMainPeople));
            }
            else
            {
                sbReferenceString.Append("未知出版者");
            }
            sbReferenceString.Append(".");
            sbReferenceString.Append(" ");

            //题名 文献类型标志
            if (!string.IsNullOrWhiteSpace(strContributionTitle))
            {
                sbReferenceString.Append(strContributionTitle);
            }
            else
            {
                sbReferenceString.Append("未知文献");
            }

            sbReferenceString.Append(" ");

            //文献类型标志（文献类型/文献载体）
            sbReferenceString.Append("[");

            string strType = strLiteratureType.Substring(strLiteratureType.IndexOf('[') + 1, strLiteratureType.IndexOf(']') - 1);
            string strCarrier = strLiteratureCarrier.Substring(strLiteratureCarrier.IndexOf('[') + 1, strLiteratureCarrier.IndexOf(']') - 1);

            sbReferenceString.Append(strType);
            if (!string.IsNullOrWhiteSpace(strCarrier))
            {
                sbReferenceString.Append("/");
                sbReferenceString.Append(strCarrier);
            }

            sbReferenceString.Append("].");
            sbReferenceString.Append(" ");

            //其他责任者
            if (!string.IsNullOrWhiteSpace(strOtherPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strOtherPeople));
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //专著主要责任者 专著题名 其他题名信息
            if (!string.IsNullOrWhiteSpace(strMonoTitle))
            {
                if (!string.IsNullOrWhiteSpace(strMonoMainPeople))
                {
                    sbReferenceString.Append(NormalizePeopleList(strMonoMainPeople));
                }
                else
                {
                    sbReferenceString.Append("未知出版者");
                }

                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");

                sbReferenceString.Append(strMonoTitle);

                if (!string.IsNullOrWhiteSpace(strOtherTitleInfo))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strOtherTitleInfo);
                }

                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //版本项
            if (!string.IsNullOrWhiteSpace(strVersion))
            {
                sbReferenceString.Append(strVersion);
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //出版地 出版者 出版年（出版者存在出版地和出版年才有意义）
            if (!string.IsNullOrWhiteSpace(strPubPeople))
            {
                //出版地（可选，所以冒号放在它里面）
                if (!string.IsNullOrWhiteSpace(strPubPlace))
                {
                    sbReferenceString.Append(strPubPlace);
                    sbReferenceString.Append(":");
                }

                sbReferenceString.Append(strPubPeople);
                sbReferenceString.Append(",");
                
                if (!string.IsNullOrWhiteSpace(strPubYear))
                {
                    sbReferenceString.Append(strPubYear);
                }

                //引用页码（可选，所以冒号放在它里面）
                if (!string.IsNullOrWhiteSpace(strRefPage))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strRefPage);
                    sbReferenceString.Append(".");
                }

                sbReferenceString.Append(" ");
            }

            //引用时间
            if (!string.IsNullOrWhiteSpace(strRefTime))
            {
                sbReferenceString.Append("[");
                sbReferenceString.Append(strRefTime);
                sbReferenceString.Append("]");
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //获取和访问路径
            if (!string.IsNullOrWhiteSpace(strPath))
            {
                sbReferenceString.Append(strPath);
                sbReferenceString.Append(".");
            }

            return sbReferenceString.ToString();
        }
    }

    #endregion
    #region 连续出版物

    /// <summary>
    /// 连续出版物
    /// </summary>
    public class RefSerial : Reference
    {
        public string strMainPeople;
        public string strTitle;
        public string strTitleTip;
        public string strLiteratureType;
        public string strLiteratureCarrier;
        public string strPosition;
        public string strPubPlace;
        public string strPubPeople;
        public string strPubYear;
        public string strRefTime;
        public string strPath;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sbReferenceString = new StringBuilder();

            //主要责任者
            if (!string.IsNullOrWhiteSpace(strMainPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strMainPeople));
            }
            else
            {
                sbReferenceString.Append("未知出版者");
            }
            sbReferenceString.Append(".");
            sbReferenceString.Append(" ");

            //题名
            if (!string.IsNullOrWhiteSpace(strTitle))
            {
                sbReferenceString.Append(strTitle);

                //其他题名信息
                if (!string.IsNullOrWhiteSpace(strTitleTip))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strTitleTip);
                }
            }
            else
            {
                sbReferenceString.Append("未知文献");
            }

            sbReferenceString.Append(" ");

            //文献类型标志（文献类型/文献载体）
            sbReferenceString.Append("[");

            string strType = strLiteratureType.Substring(strLiteratureType.IndexOf('[') + 1, strLiteratureType.IndexOf(']') - 1);
            string strCarrier = strLiteratureCarrier.Substring(strLiteratureCarrier.IndexOf('[') + 1, strLiteratureCarrier.IndexOf(']') - 1);

            sbReferenceString.Append(strType);
            if (!string.IsNullOrWhiteSpace(strCarrier))
            {
                sbReferenceString.Append("/");
                sbReferenceString.Append(strCarrier);
            }

            sbReferenceString.Append("].");
            sbReferenceString.Append(" ");

            //年,卷(期)-年,卷(期)
            if(!string.IsNullOrWhiteSpace(strPosition))
            {
                sbReferenceString.Append(strPosition);
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //出版地 出版者 出版年 引用日期
            if (!string.IsNullOrWhiteSpace(strPubPeople))
            {
                //出版地（可选，所以冒号放在它里面）
                if (!string.IsNullOrWhiteSpace(strPubPlace))
                {
                    sbReferenceString.Append(strPubPlace);
                    sbReferenceString.Append(":");
                }

                sbReferenceString.Append(strPubPeople);
                sbReferenceString.Append(",");

                if (!string.IsNullOrWhiteSpace(strPubYear))
                {
                    sbReferenceString.Append(strPubYear);
                }

                sbReferenceString.Append(" ");
            }

            //引用时间
            if (!string.IsNullOrWhiteSpace(strRefTime))
            {
                sbReferenceString.Append("[");
                sbReferenceString.Append(strRefTime);
                sbReferenceString.Append("]");
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //获取和访问路径
            if (!string.IsNullOrWhiteSpace(strPath))
            {
                sbReferenceString.Append(strPath);
                sbReferenceString.Append(".");
            }

            return sbReferenceString.ToString();
        }
    }

    #endregion
    #region 连续出版物中的析出文献

    /// <summary>
    /// 连续出版物中的析出文献
    /// </summary>
    public class RefSerialContribution : Reference
    {
        public string strMainPeople;
        public string strTitle;
        public string strLiteratureType;
        public string strLiteratureCarrier;
        public string strSerialName;
        public string strSerialInfo;
        public string strPosition;
        public string strPubPlace;
        public string strPubPeople;
        public string strPubYear;
        public string strRefTime;
        public string strPath;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sbReferenceString = new StringBuilder();

            //主要责任者
            if (!string.IsNullOrWhiteSpace(strMainPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strMainPeople));
            }
            else
            {
                sbReferenceString.Append("未知出版者");
            }
            sbReferenceString.Append(".");
            sbReferenceString.Append(" ");

            //题名
            if (!string.IsNullOrWhiteSpace(strTitle))
            {
                sbReferenceString.Append(strTitle);
            }
            else
            {
                sbReferenceString.Append("未知文献");
            }

            sbReferenceString.Append(" ");

            //文献类型标志（文献类型/文献载体）
            sbReferenceString.Append("[");

            string strType = strLiteratureType.Substring(strLiteratureType.IndexOf('[') + 1, strLiteratureType.IndexOf(']') - 1);
            string strCarrier = strLiteratureCarrier.Substring(strLiteratureCarrier.IndexOf('[') + 1, strLiteratureCarrier.IndexOf(']') - 1);

            sbReferenceString.Append(strType);
            if (!string.IsNullOrWhiteSpace(strCarrier))
            {
                sbReferenceString.Append("/");
                sbReferenceString.Append(strCarrier);
            }

            sbReferenceString.Append("].");
            sbReferenceString.Append(" ");

            //连续出版物题名 其他题名信息
            if (!string.IsNullOrWhiteSpace(strSerialName))
            {
                sbReferenceString.Append(strSerialName);

                if(!string.IsNullOrWhiteSpace(strSerialInfo))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strSerialInfo);
                }

                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");

                //年,卷(期)-年,卷(期)
                if (!string.IsNullOrWhiteSpace(strPosition))
                {
                    sbReferenceString.Append(strPosition);
                    sbReferenceString.Append(".");
                    sbReferenceString.Append(" ");
                }
            }

            //引用时间
            if (!string.IsNullOrWhiteSpace(strRefTime))
            {
                sbReferenceString.Append("[");
                sbReferenceString.Append(strRefTime);
                sbReferenceString.Append("]");
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //获取和访问路径
            if (!string.IsNullOrWhiteSpace(strPath))
            {
                sbReferenceString.Append(strPath);
                sbReferenceString.Append(".");
            }

            return sbReferenceString.ToString();
        }
    }

    #endregion
    #region 专利文献

    /// <summary>
    /// 专利文献
    /// </summary>
    public class RefPatentDocument : Reference
    {
        public string strOwner;
        public string strTitle;
        public string strCountry;
        public string strPN;
        public string strLiteratureType;
        public string strLiteratureCarrier;
        public string strOpenTime;
        public string strRefTime;
        public string strPath;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sbReferenceString = new StringBuilder();

            //主要责任者
            if (!string.IsNullOrWhiteSpace(strOwner))
            {
                sbReferenceString.Append(NormalizePeopleList(strOwner));
            }
            else
            {
                sbReferenceString.Append("未知出版者");
            }
            sbReferenceString.Append(".");
            sbReferenceString.Append(" ");


            //专利题名 专利国别 专利号 （只有有了专利题名，专利国别和专利号）
            if (!string.IsNullOrWhiteSpace(strTitle))
            {
                sbReferenceString.Append(strTitle);

                if (!string.IsNullOrWhiteSpace(strCountry))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strCountry);
                }

                if (!string.IsNullOrWhiteSpace(strPN))
                {
                    sbReferenceString.Append(",");
                    sbReferenceString.Append(" ");
                    sbReferenceString.Append(strPN);
                }
            }
            else
            {
                sbReferenceString.Append("未知专利");
            }

            sbReferenceString.Append(" ");

            //文献类型标志（文献类型/文献载体）
            sbReferenceString.Append("[");

            string strType = strLiteratureType.Substring(strLiteratureType.IndexOf('[') + 1, strLiteratureType.IndexOf(']') - 1);
            string strCarrier = strLiteratureCarrier.Substring(strLiteratureCarrier.IndexOf('[') + 1, strLiteratureCarrier.IndexOf(']') - 1);

            sbReferenceString.Append(strType);
            if (!string.IsNullOrWhiteSpace(strCarrier))
            {
                sbReferenceString.Append("/");
                sbReferenceString.Append(strCarrier);
            }

            sbReferenceString.Append("].");
            sbReferenceString.Append(" ");

            //公告日期或公开日期
            if (!string.IsNullOrWhiteSpace(strOpenTime))
            {
                sbReferenceString.Append(strOpenTime);
                sbReferenceString.Append(" ");
            }

            //引用时间
            if (!string.IsNullOrWhiteSpace(strRefTime))
            {
                sbReferenceString.Append("[");
                sbReferenceString.Append(strRefTime);
                sbReferenceString.Append("]");
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //获取和访问路径
            if (!string.IsNullOrWhiteSpace(strPath))
            {
                sbReferenceString.Append(strPath);
                sbReferenceString.Append(".");
            }

            return sbReferenceString.ToString();
        }
    }

    #endregion
    #region 电子文献

    /// <summary>
    /// 电子文献
    /// </summary>
    public class RefElectronicDocument : Reference
    {
        public string strMainPeople;
        public string strTitle;
        public string strTitleTip;
        public string strLiteratureType;
        public string strLiteratureCarrier;
        public string strPubPlace;
        public string strPubPeople;
        public string strPubYear;
        public string strLastChange;
        public string strRefTime;
        public string strPath;

        /// <summary>
        /// 重写ToString方法
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sbReferenceString = new StringBuilder();

            //主要责任者
            if (!string.IsNullOrWhiteSpace(strMainPeople))
            {
                sbReferenceString.Append(NormalizePeopleList(strMainPeople));
            }
            else
            {
                sbReferenceString.Append("未知出版者");
            }
            sbReferenceString.Append(".");
            sbReferenceString.Append(" ");

            //题名
            if (!string.IsNullOrWhiteSpace(strTitle))
            {
                sbReferenceString.Append(strTitle);

                //其他题名信息
                if (!string.IsNullOrWhiteSpace(strTitleTip))
                {
                    sbReferenceString.Append(":");
                    sbReferenceString.Append(strTitleTip);
                }
            }
            else
            {
                sbReferenceString.Append("未知文献");
            }

            sbReferenceString.Append(" ");

            //文献类型标志（文献类型/文献载体）
            sbReferenceString.Append("[");

            string strType = strLiteratureType.Substring(strLiteratureType.IndexOf('[') + 1, strLiteratureType.IndexOf(']') - 1);
            string strCarrier = strLiteratureCarrier.Substring(strLiteratureCarrier.IndexOf('[') + 1, strLiteratureCarrier.IndexOf(']') - 1);

            sbReferenceString.Append(strType);
            if (!string.IsNullOrWhiteSpace(strCarrier))
            {
                sbReferenceString.Append("/");
                sbReferenceString.Append(strCarrier);
            }

            sbReferenceString.Append("].");
            sbReferenceString.Append(" ");

            //出版地 出版者 出版年 更新或修改日期 引用时间
            if (!string.IsNullOrWhiteSpace(strPubPeople))
            {
                //出版地（可选，所以冒号放在它里面）
                if (!string.IsNullOrWhiteSpace(strPubPlace))
                {
                    sbReferenceString.Append(strPubPlace);
                    sbReferenceString.Append(":");
                }

                sbReferenceString.Append(strPubPeople);
                sbReferenceString.Append(",");

                if (!string.IsNullOrWhiteSpace(strPubYear))
                {
                    sbReferenceString.Append(strPubYear);
                }

                if (!string.IsNullOrWhiteSpace(strLastChange))
                {
                    sbReferenceString.Append("(");
                    sbReferenceString.Append(strLastChange);
                    sbReferenceString.Append(")");
                }

                sbReferenceString.Append(" ");
            }

            //引用时间
            if (!string.IsNullOrWhiteSpace(strRefTime))
            {
                sbReferenceString.Append("[");
                sbReferenceString.Append(strRefTime);
                sbReferenceString.Append("]");
                sbReferenceString.Append(".");
                sbReferenceString.Append(" ");
            }

            //获取和访问路径
            if (!string.IsNullOrWhiteSpace(strPath))
            {
                sbReferenceString.Append(strPath);
                sbReferenceString.Append(".");
            }

            return sbReferenceString.ToString();
        }

    #endregion

    }
}
