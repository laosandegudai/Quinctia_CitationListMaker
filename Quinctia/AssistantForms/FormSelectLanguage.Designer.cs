﻿namespace Quinctia.AssistantForms
{
    partial class FormSelectLanguage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelectLanguage));
            this.cmbSelectLanguage = new System.Windows.Forms.ComboBox();
            this.lblTip = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.chkNoPrompt = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cmbSelectLanguage
            // 
            this.cmbSelectLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelectLanguage.FormattingEnabled = true;
            this.cmbSelectLanguage.Location = new System.Drawing.Point(16, 48);
            this.cmbSelectLanguage.Name = "cmbSelectLanguage";
            this.cmbSelectLanguage.Size = new System.Drawing.Size(158, 20);
            this.cmbSelectLanguage.TabIndex = 1;
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(31, 18);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(125, 12);
            this.lblTip.TabIndex = 0;
            this.lblTip.Text = "请选择你要使用的语言";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(43, 110);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(111, 28);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // chkNoPrompt
            // 
            this.chkNoPrompt.AutoSize = true;
            this.chkNoPrompt.Location = new System.Drawing.Point(23, 80);
            this.chkNoPrompt.Name = "chkNoPrompt";
            this.chkNoPrompt.Size = new System.Drawing.Size(120, 16);
            this.chkNoPrompt.TabIndex = 3;
            this.chkNoPrompt.Text = "不再提示此对话框";
            this.chkNoPrompt.UseVisualStyleBackColor = true;
            // 
            // FormSelectLanguage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 152);
            this.ControlBox = false;
            this.Controls.Add(this.chkNoPrompt);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbSelectLanguage);
            this.Controls.Add(this.lblTip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSelectLanguage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "选择语言";
            this.Load += new System.EventHandler(this.FormSelectLanguage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSelectLanguage;
        private System.Windows.Forms.Label lblTip;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox chkNoPrompt;
    }
}