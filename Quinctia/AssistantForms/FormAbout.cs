﻿/*
 * FormAbout - 关于窗口
 * 代码功能：Quinctia 的关于窗口
 * 作者名称：Tsybius2014
 * 创建时间：2015年1月17日
 * 最后修改：2015年1月17日
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.AssistantForms
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            InitializeComponent();
        }

        private void FormAbout_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
        }
        
        /// <summary>
        /// 设置语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormAbout.strTitle;
            this.txtTopic.Text = ConfigHelper.LanguageConfig.langFormAbout.strTopic;
            this.txtAbout.Text = ConfigHelper.LanguageConfig.langFormAbout.strAbout.Replace("|", "\r\n");
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormAbout.strOK;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
