﻿namespace Quinctia.AssistantForms
{
    partial class FormRefList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRefList));
            this.txtRefList = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtRefList
            // 
            this.txtRefList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRefList.Location = new System.Drawing.Point(0, 0);
            this.txtRefList.Multiline = true;
            this.txtRefList.Name = "txtRefList";
            this.txtRefList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRefList.Size = new System.Drawing.Size(568, 377);
            this.txtRefList.TabIndex = 0;
            this.txtRefList.TextChanged += new System.EventHandler(this.txtRefList_TextChanged);
            // 
            // FormRefList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 377);
            this.Controls.Add(this.txtRefList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormRefList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormRefList_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRefList;
    }
}