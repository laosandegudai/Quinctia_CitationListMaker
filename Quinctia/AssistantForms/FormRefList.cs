﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.AssistantForms
{
    public partial class FormRefList : Form
    {
        public FormRefList()
        {
            InitializeComponent();
        }

        public FormRefList(string textonboard)
        {
            InitializeComponent();
            strTextOnBoard = textonboard;
        }

        string strTextOnBoard = "";

        private void FormRefList_Load(object sender, EventArgs e)
        {
            this.txtRefList.Text = strTextOnBoard;
            this.txtRefList.Select(0, 0); //在TextBox获得焦点后取消对内容的自动全选
        }

        private void txtRefList_TextChanged(object sender, EventArgs e)
        {
            this.txtRefList.Text = strTextOnBoard;
            this.txtRefList.Select(0, 0); //在TextBox获得焦点后取消对内容的自动全选
        }
    }
}
