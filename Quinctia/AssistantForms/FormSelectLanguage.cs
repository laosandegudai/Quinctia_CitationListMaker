﻿/*
 * FormSelectLanguage - 选择语言窗口
 * 代码功能：选择在 Quinctia 中使用的语言 
 * 作者名称：Tsybius2014
 * 创建时间：2015年1月17日
 * 最后修改：2015年1月17日
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quinctia.AssistantForms
{
    public partial class FormSelectLanguage : Form
    {
        /// <summary>
        /// 存储语言的名称和包地址信息
        /// </summary>
        private DataTable dtLanguage = new DataTable();

        /// <summary>
        /// 无参数构造函数
        /// </summary>
        public FormSelectLanguage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 传入可使用的语言和对应文件的地址
        /// </summary>
        /// <param name="dtLang"></param>
        public FormSelectLanguage(DataTable dtLang)
        {
            InitializeComponent();
            dtLanguage = dtLang;
        }

        private void FormSelectLanguage_Load(object sender, EventArgs e)
        {
            SetWordsFromLangConfig();
            SetLangData();
        }

        /// <summary>
        /// 设置界面文字语言
        /// </summary>
        private void SetWordsFromLangConfig()
        {
            this.Text = ConfigHelper.LanguageConfig.langFormSelectLanguage.strTitle;
            this.lblTip.Text = ConfigHelper.LanguageConfig.langFormSelectLanguage.strLabelTip;
            this.chkNoPrompt.Text = ConfigHelper.LanguageConfig.langFormSelectLanguage.strCheckBoxNoPrompt;
            this.btnOK.Text = ConfigHelper.LanguageConfig.langFormSelectLanguage.strButtonOK;
        }

        /// <summary>
        /// 设置语言选择下拉框
        /// </summary>
        private void SetLangData()
        {
            //语言包不能为空
            if (dtLanguage.Rows.Count == 0)
            {
                MessageBox.Show(new ErrorManip.ErrorData(ConstantDict.CONSTANT_ERROR_NO_LANGUAGE).ToString());
                Application.Exit();
            }
            else
            {
                //读取各个语言包信息
                foreach (DataRow drLanguage in dtLanguage.Rows)
                {
                    this.cmbSelectLanguage.Items.Add(drLanguage[ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NAME]);

                    //下拉框默认值为当前语言包
                    if (drLanguage[ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_FILE].ToString() == ConfigHelper.QuinctiaConfig.GetCurrLangPath())
                    {
                        this.cmbSelectLanguage.Text = drLanguage[ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NAME].ToString();
                    }
                }

                this.chkNoPrompt.Checked = !ConfigHelper.QuinctiaConfig.GetNeedLanguagePrompt();
            }
        }

        /// <summary>
        /// 选中的语言所对应的文件
        /// </summary>
        private string strLanguageFile = "";
        /// <summary>
        /// 选中的语言所对应的文件
        /// </summary>
        /// <returns></returns>
        public string GetLanguageFile()
        {
            return strLanguageFile;
        }

        /// <summary>
        /// 指示不再提示这个对话框
        /// </summary>
        private bool bNoPrompt = false;
        /// <summary>
        /// 指示不再提示这个对话框
        /// </summary>
        /// <returns></returns>
        public bool NeedPrompt()
        {
            return !bNoPrompt;
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            //更新信息
            foreach (DataRow drLanguage in dtLanguage.Rows)
            {
                if (drLanguage[ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_NAME].ToString() == cmbSelectLanguage.Text)
                {
                    strLanguageFile = drLanguage[ConstantDict.CONSTANT_CONFIG_ATTRIBUTE_FILE].ToString();
                    break;
                }
            }
            bNoPrompt = chkNoPrompt.Checked;

            DialogResult = DialogResult.OK;
        }
    }
}
