﻿namespace Quinctia
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.mnsMenu = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGenerate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOther = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.tsslProgramInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grbOtherOper = new System.Windows.Forms.GroupBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnModifySelected = new System.Windows.Forms.Button();
            this.btnDeleteSelected = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.grbAddData = new System.Windows.Forms.GroupBox();
            this.btnAddElectronicDocument = new System.Windows.Forms.Button();
            this.btnAddPatentDocumentation = new System.Windows.Forms.Button();
            this.btnAddSerialContribution = new System.Windows.Forms.Button();
            this.btnAddSerial = new System.Windows.Forms.Button();
            this.btnAddMonographContribution = new System.Windows.Forms.Button();
            this.btnAddMonograph = new System.Windows.Forms.Button();
            this.grbCurrData = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvReferenceData = new System.Windows.Forms.DataGridView();
            this.pnl = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.mnsMenu.SuspendLayout();
            this.stsInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grbOtherOper.SuspendLayout();
            this.grbAddData.SuspendLayout();
            this.grbCurrData.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReferenceData)).BeginInit();
            this.pnl.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnsMenu
            // 
            this.mnsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiOther});
            this.mnsMenu.Location = new System.Drawing.Point(0, 0);
            this.mnsMenu.Name = "mnsMenu";
            this.mnsMenu.Size = new System.Drawing.Size(720, 25);
            this.mnsMenu.TabIndex = 0;
            this.mnsMenu.Text = "menuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.AutoSize = false;
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNew,
            this.tsmiOpen,
            this.tsmiSave,
            this.tsmiGenerate,
            this.toolStripSeparator2,
            this.tsmiConfig,
            this.toolStripSeparator3,
            this.tsmiExit});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.tsmiFile.Size = new System.Drawing.Size(94, 21);
            this.tsmiFile.Text = "文件";
            // 
            // tsmiNew
            // 
            this.tsmiNew.Name = "tsmiNew";
            this.tsmiNew.Size = new System.Drawing.Size(152, 22);
            this.tsmiNew.Text = "新建项目";
            this.tsmiNew.Click += new System.EventHandler(this.tsmiNew_Click);
            // 
            // tsmiOpen
            // 
            this.tsmiOpen.Name = "tsmiOpen";
            this.tsmiOpen.Size = new System.Drawing.Size(152, 22);
            this.tsmiOpen.Text = "打开项目";
            this.tsmiOpen.Click += new System.EventHandler(this.tsmiOpen_Click);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.Size = new System.Drawing.Size(152, 22);
            this.tsmiSave.Text = "保存项目";
            this.tsmiSave.Click += new System.EventHandler(this.tsmiSave_Click);
            // 
            // tsmiGenerate
            // 
            this.tsmiGenerate.Name = "tsmiGenerate";
            this.tsmiGenerate.Size = new System.Drawing.Size(152, 22);
            this.tsmiGenerate.Text = "生成清单";
            this.tsmiGenerate.Click += new System.EventHandler(this.tsmiGenerate_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // tsmiConfig
            // 
            this.tsmiConfig.Name = "tsmiConfig";
            this.tsmiConfig.Size = new System.Drawing.Size(152, 22);
            this.tsmiConfig.Text = "程序设置";
            this.tsmiConfig.Click += new System.EventHandler(this.tsmiConfig_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(152, 22);
            this.tsmiExit.Text = "退出程序";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // tsmiOther
            // 
            this.tsmiOther.AutoSize = false;
            this.tsmiOther.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiHelp,
            this.toolStripSeparator1,
            this.tsmiAbout});
            this.tsmiOther.Name = "tsmiOther";
            this.tsmiOther.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.tsmiOther.Size = new System.Drawing.Size(94, 21);
            this.tsmiOther.Text = "帮助";
            // 
            // tsmiHelp
            // 
            this.tsmiHelp.Name = "tsmiHelp";
            this.tsmiHelp.Size = new System.Drawing.Size(152, 22);
            this.tsmiHelp.Text = "帮助";
            this.tsmiHelp.Click += new System.EventHandler(this.tsmiHelp_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(152, 22);
            this.tsmiAbout.Text = "关于";
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // stsInfo
            // 
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslProgramInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 440);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.stsInfo.Size = new System.Drawing.Size(720, 22);
            this.stsInfo.TabIndex = 1;
            this.stsInfo.Text = "statusStrip1";
            // 
            // tsslProgramInfo
            // 
            this.tsslProgramInfo.Name = "tsslProgramInfo";
            this.tsslProgramInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsslProgramInfo.Size = new System.Drawing.Size(56, 17);
            this.tsslProgramInfo.Text = "版本信息";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grbOtherOper);
            this.panel1.Controls.Add(this.grbAddData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 332);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(720, 108);
            this.panel1.TabIndex = 2;
            // 
            // grbOtherOper
            // 
            this.grbOtherOper.Controls.Add(this.btnGenerate);
            this.grbOtherOper.Controls.Add(this.btnModifySelected);
            this.grbOtherOper.Controls.Add(this.btnDeleteSelected);
            this.grbOtherOper.Controls.Add(this.btnClear);
            this.grbOtherOper.Dock = System.Windows.Forms.DockStyle.Left;
            this.grbOtherOper.Location = new System.Drawing.Point(458, 0);
            this.grbOtherOper.Name = "grbOtherOper";
            this.grbOtherOper.Size = new System.Drawing.Size(260, 108);
            this.grbOtherOper.TabIndex = 1;
            this.grbOtherOper.TabStop = false;
            this.grbOtherOper.Text = "其他操作";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(134, 62);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(114, 36);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Text = "生成清单";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnModifySelected
            // 
            this.btnModifySelected.Location = new System.Drawing.Point(15, 20);
            this.btnModifySelected.Name = "btnModifySelected";
            this.btnModifySelected.Size = new System.Drawing.Size(114, 36);
            this.btnModifySelected.TabIndex = 3;
            this.btnModifySelected.Text = "修改选中";
            this.btnModifySelected.UseVisualStyleBackColor = true;
            this.btnModifySelected.Click += new System.EventHandler(this.btnModifySelected_Click);
            // 
            // btnDeleteSelected
            // 
            this.btnDeleteSelected.Location = new System.Drawing.Point(135, 20);
            this.btnDeleteSelected.Name = "btnDeleteSelected";
            this.btnDeleteSelected.Size = new System.Drawing.Size(114, 36);
            this.btnDeleteSelected.TabIndex = 2;
            this.btnDeleteSelected.Text = "删除选中";
            this.btnDeleteSelected.UseVisualStyleBackColor = true;
            this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(15, 62);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(114, 36);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "清空列表";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // grbAddData
            // 
            this.grbAddData.Controls.Add(this.btnAddElectronicDocument);
            this.grbAddData.Controls.Add(this.btnAddPatentDocumentation);
            this.grbAddData.Controls.Add(this.btnAddSerialContribution);
            this.grbAddData.Controls.Add(this.btnAddSerial);
            this.grbAddData.Controls.Add(this.btnAddMonographContribution);
            this.grbAddData.Controls.Add(this.btnAddMonograph);
            this.grbAddData.Dock = System.Windows.Forms.DockStyle.Left;
            this.grbAddData.Location = new System.Drawing.Point(0, 0);
            this.grbAddData.Name = "grbAddData";
            this.grbAddData.Size = new System.Drawing.Size(458, 108);
            this.grbAddData.TabIndex = 0;
            this.grbAddData.TabStop = false;
            this.grbAddData.Text = "添加条目";
            // 
            // btnAddElectronicDocument
            // 
            this.btnAddElectronicDocument.Location = new System.Drawing.Point(306, 62);
            this.btnAddElectronicDocument.Name = "btnAddElectronicDocument";
            this.btnAddElectronicDocument.Size = new System.Drawing.Size(141, 36);
            this.btnAddElectronicDocument.TabIndex = 5;
            this.btnAddElectronicDocument.Text = "电子文献";
            this.btnAddElectronicDocument.UseVisualStyleBackColor = true;
            this.btnAddElectronicDocument.Click += new System.EventHandler(this.btnAddElectronicDocument_Click);
            // 
            // btnAddPatentDocumentation
            // 
            this.btnAddPatentDocumentation.Location = new System.Drawing.Point(159, 62);
            this.btnAddPatentDocumentation.Name = "btnAddPatentDocumentation";
            this.btnAddPatentDocumentation.Size = new System.Drawing.Size(141, 36);
            this.btnAddPatentDocumentation.TabIndex = 4;
            this.btnAddPatentDocumentation.Text = "专利文献";
            this.btnAddPatentDocumentation.UseVisualStyleBackColor = true;
            this.btnAddPatentDocumentation.Click += new System.EventHandler(this.btnAddPatentDocumentation_Click);
            // 
            // btnAddSerialContribution
            // 
            this.btnAddSerialContribution.Location = new System.Drawing.Point(12, 62);
            this.btnAddSerialContribution.Name = "btnAddSerialContribution";
            this.btnAddSerialContribution.Size = new System.Drawing.Size(141, 36);
            this.btnAddSerialContribution.TabIndex = 3;
            this.btnAddSerialContribution.Text = "析出文献（出版物）";
            this.btnAddSerialContribution.UseVisualStyleBackColor = true;
            this.btnAddSerialContribution.Click += new System.EventHandler(this.btnAddSerialContribution_Click);
            // 
            // btnAddSerial
            // 
            this.btnAddSerial.Location = new System.Drawing.Point(306, 20);
            this.btnAddSerial.Name = "btnAddSerial";
            this.btnAddSerial.Size = new System.Drawing.Size(141, 36);
            this.btnAddSerial.TabIndex = 2;
            this.btnAddSerial.Text = "连续出版物";
            this.btnAddSerial.UseVisualStyleBackColor = true;
            this.btnAddSerial.Click += new System.EventHandler(this.btnAddSerial_Click);
            // 
            // btnAddMonographContribution
            // 
            this.btnAddMonographContribution.Location = new System.Drawing.Point(159, 20);
            this.btnAddMonographContribution.Name = "btnAddMonographContribution";
            this.btnAddMonographContribution.Size = new System.Drawing.Size(141, 36);
            this.btnAddMonographContribution.TabIndex = 1;
            this.btnAddMonographContribution.Text = "析出文献（专著）";
            this.btnAddMonographContribution.UseVisualStyleBackColor = true;
            this.btnAddMonographContribution.Click += new System.EventHandler(this.btnAddMonographContribution_Click);
            // 
            // btnAddMonograph
            // 
            this.btnAddMonograph.Location = new System.Drawing.Point(12, 20);
            this.btnAddMonograph.Name = "btnAddMonograph";
            this.btnAddMonograph.Size = new System.Drawing.Size(141, 36);
            this.btnAddMonograph.TabIndex = 0;
            this.btnAddMonograph.Text = "专著";
            this.btnAddMonograph.UseVisualStyleBackColor = true;
            this.btnAddMonograph.Click += new System.EventHandler(this.btnAddMonographs_Click);
            // 
            // grbCurrData
            // 
            this.grbCurrData.Controls.Add(this.panel2);
            this.grbCurrData.Controls.Add(this.pnl);
            this.grbCurrData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbCurrData.Location = new System.Drawing.Point(0, 25);
            this.grbCurrData.Name = "grbCurrData";
            this.grbCurrData.Size = new System.Drawing.Size(720, 307);
            this.grbCurrData.TabIndex = 3;
            this.grbCurrData.TabStop = false;
            this.grbCurrData.Text = "当前条目";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvReferenceData);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(632, 287);
            this.panel2.TabIndex = 2;
            // 
            // dgvReferenceData
            // 
            this.dgvReferenceData.AllowUserToAddRows = false;
            this.dgvReferenceData.AllowUserToDeleteRows = false;
            this.dgvReferenceData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReferenceData.ColumnHeadersVisible = false;
            this.dgvReferenceData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReferenceData.Location = new System.Drawing.Point(0, 0);
            this.dgvReferenceData.MultiSelect = false;
            this.dgvReferenceData.Name = "dgvReferenceData";
            this.dgvReferenceData.ReadOnly = true;
            this.dgvReferenceData.RowTemplate.Height = 23;
            this.dgvReferenceData.Size = new System.Drawing.Size(632, 287);
            this.dgvReferenceData.TabIndex = 0;
            this.dgvReferenceData.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvReferenceData_RowPostPaint);
            // 
            // pnl
            // 
            this.pnl.Controls.Add(this.tableLayoutPanel1);
            this.pnl.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnl.Location = new System.Drawing.Point(635, 17);
            this.pnl.Name = "pnl";
            this.pnl.Size = new System.Drawing.Size(82, 287);
            this.pnl.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnDown, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnUp, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(82, 287);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(3, 146);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(75, 80);
            this.btnDown.TabIndex = 1;
            this.btnDown.Text = "↓";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUp
            // 
            this.btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUp.Location = new System.Drawing.Point(3, 60);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(75, 80);
            this.btnUp.TabIndex = 0;
            this.btnUp.Text = "↑";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 462);
            this.Controls.Add(this.grbCurrData);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.mnsMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnsMenu;
            this.MinimumSize = new System.Drawing.Size(736, 500);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quinctia 文献引用生成工具";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.mnsMenu.ResumeLayout(false);
            this.mnsMenu.PerformLayout();
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.grbOtherOper.ResumeLayout(false);
            this.grbAddData.ResumeLayout(false);
            this.grbCurrData.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReferenceData)).EndInit();
            this.pnl.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnsMenu;
        private System.Windows.Forms.StatusStrip stsInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grbOtherOper;
        private System.Windows.Forms.GroupBox grbAddData;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnModifySelected;
        private System.Windows.Forms.Button btnDeleteSelected;
        private System.Windows.Forms.Button btnAddElectronicDocument;
        private System.Windows.Forms.Button btnAddPatentDocumentation;
        private System.Windows.Forms.Button btnAddSerialContribution;
        private System.Windows.Forms.Button btnAddSerial;
        private System.Windows.Forms.Button btnAddMonographContribution;
        private System.Windows.Forms.Button btnAddMonograph;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.ToolStripMenuItem tsmiOther;
        private System.Windows.Forms.GroupBox grbCurrData;
        private System.Windows.Forms.DataGridView dgvReferenceData;
        private System.Windows.Forms.ToolStripMenuItem tsmiNew;
        private System.Windows.Forms.ToolStripMenuItem tsmiOpen;
        private System.Windows.Forms.ToolStripMenuItem tsmiSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiGenerate;
        private System.Windows.Forms.ToolStripMenuItem tsmiHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmiConfig;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripStatusLabel tsslProgramInfo;
        private System.Windows.Forms.Panel pnl;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnClear;
    }
}

